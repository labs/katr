import os
from katr.reviews import models
from katr.routers import models as rout_mod
from django.conf import settings

os.environ['DJANGO_SETTINGS_MODULE'] = 'katr.settings'


def remove_texts():
    deleted = 0
    kept = 0
    for rev in models.RouterReview.objects.all():
        if rev.text_en.startswith("Lorem"):
            for lang in settings.LANGUAGES:
                for field in ('text', 'summary'):
                    setattr(rev, field + "_" + lang[0], "")
            rev.save()
            deleted += 1
        else:
            kept += 1
    print "Text cleared from {0}, unchanged {1}".format(deleted, kept)

# remove_texts()


def debug_pict():
    for pict in rout_mod.RouterPicture.objects.all():
        print pict.get_XXXAS()

debug_pict()
