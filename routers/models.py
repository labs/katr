# Django imports
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.core.files.base import ContentFile

# built-in modules
import os.path
import urllib2
import imghdr

# third party libs
from autoslug import AutoSlugField
from transmeta import TransMeta
from PIL import Image
from StringIO import StringIO


class Router(models.Model):

    class Meta:
        unique_together = (('manufacturer', 'slug'))
        ordering = ["name"]

    manufacturer = models.ForeignKey('Manufacturer')
    slug = AutoSlugField(populate_from='name', unique_with="manufacturer")
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=64, default="", blank=True)
    hardware_revision = models.CharField(max_length=64, default="", blank=True)

    def __unicode__(self):
        ret = u"{0} {1}".format(self.manufacturer, self.name)
        if self.hardware_revision:
            ret += u" rev. {0}".format(self.hardware_revision)
        if self.code:
            ret += u"({0})".format(self.code)
        return ret

    def first_picture(self):
        try:
            return self.routerpicture_set.get(number=1)
        except RouterPicture.DoesNotExist:
            return None

    def download_picture(self, url, title=None):
        number = self.routerpicture_set.aggregate(
            models.Max('number'))['number__max']
        if number:
            number += 1
        else:
            # first picture
            number = 1
        picture = RouterPicture(
            router=self, number=number, url=url, title=title)
        try:
            picture.fetch_from_url()
            picture.save()
        except urllib2.HTTPError as err:
            print 'HTTP error {0}: {1}. Picture was not downloaded'.format(err.code, err.reason)

    def add_local_picture(self, path, title=None):
        number = self.routerpicture_set.aggregate(
            models.Max('number'))['number__max']
        if number:
            number += 1
        else:
            # first picture
            number = 1
        picture = RouterPicture(router=self, number=number, title=title)
        picture.fetch_from_url(url=path, local=True)
        picture.save()


class RouterFirmware(models.Model):

    class Meta:
        unique_together = (('router', 'name'))
        ordering = ["name"]

    router = models.ForeignKey('Router')
    name = models.CharField(max_length=255, default="", blank=True)

    def __unicode__(self):
        return u"{1}, firmware '{0}'".format(self.name, self.router)


class Manufacturer(models.Model):

    class Meta:
        ordering = ["name"]

    slug = AutoSlugField(populate_from='name', unique=True)
    name = models.CharField(max_length=255)
    website = models.URLField()

    def __unicode__(self):
        return u"{0}".format(self.name)


class RouterPicture(models.Model):
    __metaclass__ = TransMeta

    class RouterPictureException(Exception):
        pass

    class Meta:
        translate = ("title",)
        unique_together = ('router', 'number')
        ordering = ["number"]

    PICT_SUBDIR_DIR = "router_picts"
    PICT_DIR = settings.STATIC_ROOT + PICT_SUBDIR_DIR
    THUMBS_SUBDIR_DIR = "router_thumbs"
    THUMBS_DIR = settings.STATIC_ROOT + THUMBS_SUBDIR_DIR
    THUMB_MEDIUM_SIDE = 264
    THUMB_SMALL_SIDE = 76

    router = models.ForeignKey('Router')
    number = models.PositiveIntegerField(verbose_name=_("Number"))
    title = models.CharField(verbose_name=_("Title"), max_length=255,
                             blank=True)
    url = models.URLField(verbose_name=_("Picture URL"), blank=True)
    pict_file = models.FileField(verbose_name=_("Picture file"), blank=True,
                                 upload_to=PICT_SUBDIR_DIR)
    thumb_medium_file = models.FileField(verbose_name=_("Thumbnail medium"), blank=True,
                                         upload_to=THUMBS_SUBDIR_DIR)
    thumb_small_file = models.FileField(verbose_name=_("Thumbnail small"), blank=True,
                                        upload_to=THUMBS_SUBDIR_DIR)

    def __unicode__(self):
        text = u"Pict #{0} of {1}".format(self.number, self.router)
        if self.title:
            text += u" ({0})".format(self.title)
        return text

    def save(self, **kwargs):
        try:
            old = RouterPicture.objects.get(id=self.id)
            if old.pict_file != self.pict_file and old.pict_file:
                old.pict_file.delete(save=False)
        except RouterPicture.DoesNotExist:
            pass
        if self.url and not self.pict_file:
            self.fetch_from_url()
        models.Model.save(self, **kwargs)
        if self.pict_file:
            self.create_thumbnail('medium')
            self.create_thumbnail('small')
            models.Model.save(self, **kwargs)
        #super(RouterPicture, self).save(self, **kwargs)

    def fetch_from_url(self, url=None, local=False):
        if not url:
            url = self.url
        if local:
            furl = open(url, 'r')
        else:
            furl = urllib2.urlopen(url, timeout=10)
        file_ext = os.path.splitext(url)[1]
        img_filename = "{0}-pict-{1}{2}".format(self.router.slug, self.number,
                                                file_ext)
        try:
            img_temp = NamedTemporaryFile(delete=True)
            img_temp.write(furl.read())
            img_temp.flush()
            img_type = imghdr.what(img_temp.name)
            if img_type is None:
                raise self.RouterPictureException(
                    "Unknown image type - probably corrupted data")
            # if there is no extension in url, use the one provided by imghdr
            if not file_ext:
                img_filename += "." + img_type
            self.pict_file.save(img_filename, File(img_temp), save=False)
        finally:
            furl.close()
        img_type = imghdr.what(self.get_file_path())

    def create_thumbnail(self, size='small'):
        if size == 'medium':
            side = self.THUMB_MEDIUM_SIDE
        elif size == 'small':
            side = self.THUMB_SMALL_SIDE
        else:
            return
        with open(self.get_file_path(), 'r') as fr:
            thumb_size = (side, side)
            large_image = Image.open(fr, 'r')
            large_image.thumbnail(thumb_size, Image.ANTIALIAS)
            buffer = StringIO()
            # save thumbnail to buffer
            large_image.save(buffer, format=large_image.format)
            # create django File-like object from buffer
            thumb_file = ContentFile(buffer.getvalue())
            filename = size + '-' + self.pict_file.name.split('/')[-1]
            if size == 'medium':
                if self.thumb_medium_file:
                    path = self.get_thumb_medium_path()
                    self.thumb_medium_file.delete(save=False)
                self.thumb_medium_file.save(
                    filename, File(thumb_file), save=False)
            elif size == 'small':
                if self.thumb_small_file:
                    path = self.get_thumb_small_path()
                    self.thumb_small_file.delete(save=False)
                self.thumb_small_file.save(
                    filename, File(thumb_file), save=False)
            else:
                return
        img_type = imghdr.what(self.get_file_path())

    def get_absolute_url(self):
        return settings.MEDIA_URL + self.pict_file.name

    def get_file_path(self):
        return os.path.join(settings.MEDIA_ROOT, self.pict_file.name)

    def get_absolute_thumb_medium_url(self):
        return settings.MEDIA_URL + self.thumb_medium_file.name

    def get_thumb_medium_path(self):
        return os.path.join(settings.MEDIA_ROOT, self.thumb_medium_file.name)

    def get_absolute_thumb_small_url(self):
        return settings.MEDIA_URL + self.thumb_small_file.name

    def get_thumb_small_path(self):
        return os.path.join(settings.MEDIA_ROOT, self.thumb_small_file.name)
