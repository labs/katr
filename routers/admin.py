import models
from django.contrib import admin
from django.utils.safestring import mark_safe
from django.core import urlresolvers
from django.db import models as dbmodels
from django.forms import TextInput


class RouterPictureInline(admin.TabularInline):
    exclude = ('thumb_medium_file', 'thumb_small_file', 'url',)
    model = models.RouterPicture
    extra = 1
    formfield_overrides = {
        dbmodels.CharField: {'widget': TextInput(attrs={'size': '20'})},
    }
    readonly_fields = ['routerpicture_link']

    def routerpicture_link(self, obj):
        if not obj.id:
            return 'Add number and new picture and save.'
        change_url = urlresolvers.reverse(
            'admin:routers_routerpicture_change', args=(obj.id,))
        return mark_safe('<a href="%s">%s</a>' % (change_url, obj))
    routerpicture_link.short_description = 'Details'


class RouterPictureAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'number', 'router_link')
    list_filter = ('router',)
    exclude = ('thumb_medium_file', 'thumb_small_file',)
    readonly_fields = ['router_link']

    def router_link(self, obj):
        change_url = urlresolvers.reverse(
            'admin:routers_router_change', args=(obj.router.id,))
        return mark_safe('<a href="%s">%s</a>' % (change_url, obj.router))
    router_link.short_description = 'Router'


class RouterAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'name', 'manufacturer', 'picture_count')
    list_filter = ('manufacturer',)
    inlines = [RouterPictureInline]

    def picture_count(self, obj):
        return obj.routerpicture_set.count()
    picture_count.short_description = 'Number of Pictures'


admin.site.register(models.Router, RouterAdmin)
admin.site.register(models.Manufacturer)
admin.site.register(models.RouterFirmware)
admin.site.register(models.RouterPicture, RouterPictureAdmin)
