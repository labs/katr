import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'katr.settings'

from django.core import serializers

static_texts_file = "static_content/fixtures/static_texts.yaml"

with file(static_texts_file, 'r') as yaml_file:
    data = yaml_file.read()
    for obj in serializers.deserialize('yaml', data):
        obj.save()
