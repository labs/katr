import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'katr.settings'

from properties import models
import logging
logging.basicConfig(level=logging.DEBUG)
import csv
import yaml

#
# Quantities
#
quant_file = "data/quantities.csv"
logging.info("Importing quantities from '%s'", quant_file)
seen_quantities = set()
with file(quant_file, 'r') as csv_file:
    csv_reader = csv.reader(csv_file)
    csv_reader.next()  # skip the header
    for row in csv_reader:
        name, type_name, unit, note = row
        seen_quantities.add(name)
        unit = unit or None
        value_type = models.Quantity.type_name_to_int(type_name)
        try:
            obj = models.Quantity.objects.get(name=name)
        except models.Quantity.DoesNotExist:
            # we will create a new one below
            obj = None
        else:
            # we have it already, just sync it
            if obj.value_type == value_type:
                if obj.unit == unit:
                    # its it ok, we do not need to do anything
                    pass
                    # logging.debug("Quantity '%s' does not need any update",
                    #              name)
                else:
                    # we can just update it
                    logging.debug("Updating unit for quantity '%s'", name)
                    obj.unit = unit
                    obj.save()
            else:
                # we need to delete it because the data would be inconsistent
                # otherwise
                logging.debug("Deleting quantity '%s' for synchronization",
                              name)
                obj.delete()
                obj = None
        if obj is None:
            # we create a new one
            logging.debug("Creating quantity '%s'", name)
            obj = models.Quantity(name=name, unit=unit, value_type=value_type)
            obj.save()
# clean up quantities
for quantity in models.Quantity.objects.exclude(name__in=seen_quantities):
    logging.debug("Deleting stale quantity '%s'", quantity.name)
    quantity.delete()

#
# Properties
#
prop_file = "data/properties.csv"
logging.info("Importing properties from '%s'", prop_file)
seen_properties = set()
with file(prop_file, 'r') as csv_file:
    csv_reader = csv.reader(csv_file)
    csv_reader.next()  # skip the header
    for row in csv_reader:
        name, local_name_en, local_name_cs, quantity = [x.decode('utf-8')
                                                        for x in row[:4]]
        comment_en, comment_cs = "", ""
        if len(row) > 4:
            comment_en = row[4].decode('utf-8')
        if len(row) > 5:
            comment_cs = row[5].decode('utf-8')
        else:
            logging.warn("Missing comment for lang 'cs' for '%s', using 'en' "
                         "value", name)
            comment_cs = comment_en

        seen_properties.add(name)
        # see if we already have it
        try:
            obj = models.Property.objects.get(name=name)
        except models.Property.DoesNotExist:
            obj = None
        else:
            # we have it, let's sync it
            if obj.quantity.name == quantity:
                # no need to delete it - go on with sync
                if local_name_cs == obj.local_name_cs and \
                   local_name_en == obj.local_name_en and \
                   comment_en == obj.local_comment_en and \
                   comment_cs == obj.local_comment_cs:
                    pass
                    # logging.debug("Property '%s' does not need any update",
                    #              name)
                else:
                    logging.debug("Updating property '%s'", name)
                    # logging.debug("Updating property '%s', '%s'",
                    # type(obj.local_name_cs), type(local_name_cs))
                    obj.local_name_cs = local_name_cs
                    obj.local_name_en = local_name_en
                    obj.local_comment_cs = comment_cs
                    obj.local_comment_en = comment_en
                    obj.save()
            else:
                logging.debug("Deleting property '%s' for synchronization",
                              name)
                obj.delete()
                obj = None
        if obj is None:
            # create a new property instance
            quantity_obj = models.Quantity.objects.get(name=quantity)
            obj = models.Property(name=name,
                                  quantity=quantity_obj,
                                  local_name_en=local_name_en,
                                  local_name_cs=local_name_cs,
                                  local_comment_en=comment_en,
                                  local_comment_cs=comment_cs)
            logging.debug("Created property '%s'", name)
            obj.save()
# clean up properties
for prop in models.Property.objects.exclude(name__in=seen_properties):
    logging.debug("Deleting stale property '%s'", prop.name)
    prop.delete()

#
# PropertyLists
#
prop_list_file = "data/property_lists.yaml"
logging.info("Importing property lists from '%s'", prop_list_file)
with file(prop_list_file, 'r') as json_file:
    defs = yaml.load(json_file)
seen_prop_lists = set()
for desc in defs:
    # delete possible old data
    models.PropertyList.objects.filter(name=desc.get('name')).delete()
    seen_prop_lists.add(desc.get('name'))
    # create a new property instance
    obj = models.PropertyList()
    # non-localizable stuff first, except properties, which are many-to-many
    for attr, val in desc.iteritems():
        if not attr.startswith("local_") and \
           attr not in ('properties', 'child_sets', 'bound_property'):
            setattr(obj, attr, val)
    # now create a set of values for each language
    lang_to_attrs = {}
    for attr, val in desc.iteritems():
        if attr.startswith("local_"):
            for lang, val2 in val.iteritems():
                if lang not in lang_to_attrs:
                    lang_to_attrs[lang] = {}
                lang_to_attrs[lang][attr] = val2
    # not put in the localizable values
    for lang, attrs in lang_to_attrs.iteritems():
        # obj.translate(lang)
        for attr, val2 in attrs.iteritems():
            setattr(obj, attr + "_" + lang, val2)
    # bound property
    bound_prop_name = desc.get('bound_property')
    if bound_prop_name:
        obj.bound_property = models.Property.objects.get(name=bound_prop_name)
    obj.save()
    # properties - have to be added after save, in order to have pk available
    for i, prop in enumerate(desc.get('properties', [])):
        prop_obj = models.Property.objects.get(name=prop)
        ps2p = models.PropertyListToProperty(property=prop_obj,
                                             property_list=obj,
                                             position=i)
        ps2p.save()
    # parent-child relationships - the same as with properties applies
    for i, child in enumerate(desc.get('child_sets', [])):
        logging.debug("Adding child '%s' to '%s'", child, obj)
        child_obj = models.PropertyList.objects.get(name=child)
        ps2ps = models.PropertyListParentToChild(child=child_obj, parent=obj,
                                                 position=i)
        ps2ps.save()
# clean up propertylists
for prop in models.PropertyList.objects.exclude(name__in=seen_prop_lists):
    logging.debug("Deleting stale property list '%s'", prop.name)
    prop.delete()

#
# checks of the final structure
#


def dive(parent, seen_props, seen_ids):
    if parent.pk in seen_ids:
        logging.error("There is a loop in PropertyList structure!")
        raise Exception()
    seen_ids.add(parent.pk)
    for ps2p in parent.propertylisttoproperty_set.select_related().all():
        seen_props.add(ps2p.property.name)
    if parent.bound_property:
        seen_props.add(parent.bound_property.name)
    for child_link in parent.childlink_set.select_related().all():
        child = child_link.child
        dive(child, seen_props, seen_ids)

logging.info("Checking property list structure")
try:
    full_review = models.PropertyList.objects.get(name="full_review")
except models.PropertyList.DoesNotExist:
    logging.error("Property list with name 'full_review' does not exist!")
else:
    # full_review is there, lets test that all properties are reachable from it
    seen_ids = set()
    seen_props = set()
    dive(full_review, seen_props, seen_ids)
    known_properties = set(models.Property.get_name_to_prop_map().keys())
    for name in (known_properties - seen_props):
        logging.warn("Property unreachable from 'full_review': '%s'", name)
