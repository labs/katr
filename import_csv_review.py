import logging
import sys
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'katr.settings'

from reviews.review_import import import_csv_file

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger("import")
    for fname in sys.argv[1:]:
        with file(fname, 'r') as csv_file:
            import_csv_file(csv_file, logger)
