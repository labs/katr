from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

handler500 = 'reviews.views.error500'

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'katr.views.home', name='home'),
    url("^", include('reviews.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # following lines enable password reset
    url(r'^admin/password_reset/', include([
        url(r'^$', 'django.contrib.auth.views.password_reset',
            name='admin_password_reset'),
        url(r'^done/$', 'django.contrib.auth.views.password_reset_done',
            name='password_reset_done'),
        url(r'^confirm/(?P<uidb64>[0-9A-Za-z_\-]+)-(?P<token>.+)/$',
            'django.contrib.auth.views.password_reset_confirm', name='password_reset_confirm'),
        url(r'^completed/$', 'django.contrib.auth.views.password_reset_complete',
            name='password_reset_complete'),
    ])),
    # Uncomment the next line to enable the admin:
    url('^admin/', include(admin.site.urls)),

    ('^media/(?P<path>.*)$', 'django.views.static.serve',
                             {'document_root': 'media'}),
)
