"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

# built-in modules
from datetime import datetime

# django imports
from django.test import TestCase
from django.conf import settings
from django.utils.translation import activate

# local imports
from models import TypedValue, PropertyList, PropertyListParentToChild, \
    Property, PropertyListToProperty, PropertyValue, \
    PropertyValueCollection, Quantity

#from django.db import connection
#connection.creation.destroy_test_db = lambda *args, **kwargs: None


class TypedValueStorageTest(TestCase):

    class TypedValueSimpleImp(TypedValue):
        pass

    test_data = ((TypedValue.TYPE_INT, 100),
                 (TypedValue.TYPE_FLOAT, 100.3),
                 (TypedValue.TYPE_TEXT, 'This is text'),
                 (TypedValue.TYPE_IP_ADDR, '127.0.1.2'),
                 (TypedValue.TYPE_URL, 'http://www.nic.cz'),
                 (TypedValue.TYPE_LOCAL_TEXT, 'local_text'),
                 (TypedValue.TYPE_BOOL, True),
                 (TypedValue.TYPE_DATETIME, datetime(2012, 7, 7, 13, 56, 44))
                 )

    def test_storage(self):
        """
        Tests that it is ok to save values in a typed model and retrieve it
        unchanged
        """
        # storage
        for type_num, value in self.test_data:
            tv = self.TypedValueSimpleImp(value_type=type_num)
            tv.set_value(value)
            tv.save()
            self.assertEqual(value, tv.get_value())
            self.assertTrue(tv.validate())
        # retrieval
        for type_num, value in self.test_data:
            tv = self.TypedValueSimpleImp.objects.get(value_type=type_num)
            self.assertEqual(value, tv.get_value())


class TypedValueLocalizationTest(TestCase):

    class TypedValueSimpleImp(TypedValue):
        pass

    def test_localization1(self):
        """
        Test that localization of values works as expected
        """
        # different values
        for lang in settings.LANGUAGES:
            activate(lang[0])
            val = "val in lang %s" % lang[0]
            tv = self.TypedValueSimpleImp(
                value_type=TypedValue.TYPE_LOCAL_TEXT)
            tv.set_value(val)
            self.assertEqual(val, tv.get_value())

    def test_localization2(self):
        # languages for the saved object
        tv = self.TypedValueSimpleImp(value_type=TypedValue.TYPE_LOCAL_TEXT)
        for lang in settings.LANGUAGES:
            activate(lang[0])
            val = "val in lang %s" % lang[0]
            tv.set_value(val)
            self.assertEqual(val, tv.get_value())
        tv.save()
        # retrieval
        tv_id = tv.pk
        tv2 = self.TypedValueSimpleImp.objects.get(pk=tv_id)
        for lang in settings.LANGUAGES:
            activate(lang[0])
            val = "val in lang %s" % lang[0]
            self.assertEqual(val, tv2.get_value())


class PropertyListLinkingTest(TestCase):

    def setUp(self):
        ps1 = PropertyList(name='set1')
        ps2 = PropertyList(name='set2')
        ps1.save()
        ps2.save()
        self.ps1 = ps1
        self.ps2 = ps2
        link1 = PropertyListParentToChild(parent=self.ps1, child=self.ps2,
                                          position=1)
        link1.save()

    def test_linking(self):
        # count the links
        self.assertEqual(len(self.ps1.childlink_set.all()), 1)
        self.assertEqual(len(self.ps2.childlink_set.all()), 0)
        self.assertEqual(len(self.ps1.parentlink_set.all()), 0)
        self.assertEqual(len(self.ps2.parentlink_set.all()), 1)
        # check values
        self.assertEqual(self.ps1.childlink_set.all()[0].child.name, 'set2')
        self.assertEqual(self.ps1.childlink_set.all()[0].parent.name, 'set1')
        self.assertEqual(self.ps2.parentlink_set.all()[0].parent.name, 'set1')

    def test_traversal(self):
        self.ps1.retrieve_child_structure()
        self.assertEqual(len(self.ps1.child_objects), 1)
        self.assertEqual(self.ps1.child_objects[0].name, 'set2')
        self.assertEqual(len(self.ps1.child_objects[0].child_objects), 0)

    def test_traversal_loop1(self):
        # create backwards link
        PropertyListParentToChild(parent=self.ps2, child=self.ps1,
                                  position=1).save()
        self.assertRaises(PropertyList.LoopException,
                          self.ps1.retrieve_child_structure)

    def test_traversal_loop2(self):
        # create backwards link
        PropertyListParentToChild(parent=self.ps2, child=self.ps1,
                                  position=1).save()
        self.ps1.retrieve_child_structure(crash_on_loop=False)
        # should be a spanning tree
        self.assertEqual(len(self.ps1.child_objects), 1)
        self.assertEqual(self.ps1.child_objects[0].name, 'set2')
        self.assertEqual(len(self.ps1.child_objects[0].child_objects), 0)


class PropertyValueCollectionTest(TestCase):

    VALUES = (3.1415927, 'TEXT', True)

    def setUp(self):
        # setup properties and property sets
        q1 = Quantity(name="q_float", value_type=Quantity.TYPE_FLOAT)
        q1.save()
        q2 = Quantity(name="q_text", value_type=Quantity.TYPE_TEXT)
        q2.save()
        q3 = Quantity(name="q_bool", value_type=Quantity.TYPE_BOOL)
        q3.save()
        prop1 = Property(name="prop1", quantity=q1)
        prop2 = Property(name="prop2", quantity=q2)
        prop3 = Property(name="prop3", quantity=q3)
        prop1.save()
        prop2.save()
        prop3.save()
        self.property_map = {prop1.name: prop1.id,
                             prop2.name: prop2.id,
                             prop3.name: prop3.id}
        ps1 = PropertyList(name='set1')
        ps2 = PropertyList(name='set2')
        ps1.save()
        ps2.save()
        PropertyListToProperty(property_list=ps1, property=prop1,
                               position=10).save()
        PropertyListToProperty(property_list=ps1, property=prop2,
                               position=20).save()
        PropertyListToProperty(property_list=ps2, property=prop3).save()
        PropertyListParentToChild(parent=ps1, child=ps2, position=1).save()
        # setup some values
        pvc1 = PropertyValueCollection()
        pvc1.save()
        pv1 = PropertyValue(value_type=TypedValue.TYPE_FLOAT, property=prop1,
                            collection=pvc1)
        pv1.set_value(self.VALUES[0])
        pv1.save()
        pv2 = PropertyValue(value_type=TypedValue.TYPE_TEXT, property=prop2,
                            collection=pvc1)
        pv2.set_value(self.VALUES[1])
        pv2.save()
        pv3 = PropertyValue(value_type=TypedValue.TYPE_BOOL, property=prop3,
                            collection=pvc1)
        pv3.set_value(self.VALUES[2])
        pv3.save()
        self.coll = pvc1

    def test_value_list_creation_ids(self):
        res = self.coll.create_prop_id_to_value_dict()
        self.assertEqual(type(res), dict)
        self.assertEqual(res[self.property_map['prop1']], self.VALUES[0])
        self.assertEqual(res[self.property_map['prop2']], self.VALUES[1])
        self.assertEqual(res[self.property_map['prop3']], self.VALUES[2])

    def test_value_list_creation_names(self):
        res = self.coll.create_prop_name_to_value_dict()
        self.assertEqual(type(res), dict)
        self.assertEqual(res['prop1'], self.VALUES[0])
        self.assertEqual(res['prop2'], self.VALUES[1])
        self.assertEqual(res['prop3'], self.VALUES[2])

    def test_collection_set_population(self):
        prop_set = PropertyList.objects.get(name="set1")
        self.coll.fill_in_property_list(prop_set)
        self.assert_(hasattr(prop_set, "property_values"))
        self.assertEqual(type(prop_set.property_values), list)
        for i, prop in enumerate(prop_set.property_objects):
            val = prop_set.property_values[i]
            self.assertEqual(prop.name, "prop%d" % (i + 1))
            self.assertEqual(val, self.VALUES[i])
