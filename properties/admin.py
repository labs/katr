import models
from django.contrib import admin

admin.site.register(models.Property)
admin.site.register(models.PropertyList)
admin.site.register(models.PropertyValue)
admin.site.register(models.Quantity)
admin.site.register(models.PropertyValueCollection)
