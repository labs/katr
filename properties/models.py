# Django imports
from django.db import models
from django.utils.translation import get_language, ugettext_lazy as _

# third party libs
from transmeta import TransMeta

#
# Following are models describing an abstract structure of properties
#


class PropertyList(models.Model):
    """
    Describes a set of properties, that are somehow related.
    """
    __metaclass__ = TransMeta

    class LoopException(Exception):
        pass

    class Meta:
        translate = ('local_name', 'local_comment')
        ordering = ["name"]

    name = models.SlugField(max_length=32, unique=True)
    local_name = models.CharField(blank=True, max_length=255,
                                  verbose_name=_("Localized name"), default='')
    local_comment = models.TextField(
        blank=True, verbose_name=_("Localized comment"), default='')
    children = models.ManyToManyField('self',
                                      through='PropertyListParentToChild',
                                      symmetrical=False)
    properties = models.ManyToManyField('Property',
                                        through='PropertyListToProperty')
    bound_property = models.ForeignKey('Property', null=True,
                                       related_name="boundpropertylist_set")
    collapsible = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    def retrieve_child_structure(self, retrieve_properties=False,
                                 crash_on_loop=True):
        """
        Follows all children links in recursive manner and creates a tree
        of property sets.
        In case the structure contains a loop, the behavior depends on
        :py:attr:crash_on_loop - if True, exception is raised, otherwise a
        spanning tree will be created instead of the complete graph.
        The structure is created by adding :py:attr:child_objects to all
        children which is a list of child PropertyList instances

        Args:
            crash_on_loop (bool) - controls loop detection behavior
            retrieve_properties (bool) - should :py:meth:retrieve_properties
                                        be called on each child PropertyList?

        Side effects:
            creates and populates attribute :py:attr:child_objects
        """
        seen_ids = set()

        def dive(parent):
            if parent.pk in seen_ids:
                # we use this for loop detection
                if crash_on_loop:
                    raise self.LoopException(
                        'Loop in child structure detected')
                else:
                    return False  # signals stop not to recreate the loops
            seen_ids.add(parent.pk)
            if retrieve_properties:
                parent.retrieve_properties()
            parent.child_objects = []
            for child_link in parent.childlink_set.select_related().all()\
                    .order_by("position"):
                child = child_link.child
                add_it = dive(child)
                if add_it:
                    parent.child_objects.append(child)
            return True
        dive(self)

    def get_flat_child_structure(self):
        """
        Return a table like structure describing the property list structure
        rooted at this node

        Returns:
            [property|propertlyList, is_parent, level, property],
            ...]
            (the property at [0] is used for naming this row in table,
            while the property at [3] is intended to value retrieval
             - it is None if no value should be there and reflects the
               bound_property attribute, it can be a list in case of
               collapsible lists)

        Side effects:
            runs :py:method:retrieve_child_structure on the prop_list
        """
        self.retrieve_child_structure(retrieve_properties=True)
        result = []

        def fill_it(pset, level):
            # put self into the list
            if pset.bound_property:
                props = pset.bound_property
                parent = True
            elif pset.collapsible:
                # we inline all True values
                props = []
                for prop in pset.property_objects:
                    props.append(prop)
                parent = False
            else:
                props = None
                parent = True
            result.append([pset, parent, level, props])  # True means parent
            # process children
            for child in pset.child_objects:
                fill_it(child, level + 1)
            # process child properties
            if not pset.collapsible:
                for prop in pset.property_objects:
                    result.append([prop, False, level + 1, prop])

        fill_it(self, 0)
        return result

    def retrieve_properties(self):
        """
        Similarly to :py:meth:retrieve_child_structure creates an extra
        attr :py:attr:property_objects populated with related property instances

        Side effects:
            creates and populates attribute :py:attr:property_objects
        """
        self.property_objects = []
        for ps2p in self.propertylisttoproperty_set.select_related().all()\
                .order_by('position'):
            self.property_objects.append(ps2p.property)


class PropertyListParentToChild(models.Model):
    """
    A many-to-many relationship model to allow additional attributes on
    the connection.
    """

    class Meta:
        unique_together = (('parent', 'child'))

    parent = models.ForeignKey('PropertyList', related_name="childlink_set")
    child = models.ForeignKey('PropertyList', related_name="parentlink_set")
    position = models.PositiveIntegerField(default=1)

    def __unicode__(self):
        return u"PropertyList parent-child link between {0} - {1}".format(
            self.parent, self.child)


class PropertyListToProperty(models.Model):
    """
    A many-to-many relationship model to allow additional attributes on
    the connection.
    """

    class Meta:
        unique_together = (('property_list', 'property'))

    property_list = models.ForeignKey('PropertyList')
    property = models.ForeignKey('Property')
    position = models.PositiveIntegerField(default=1)

    def __unicode__(self):
        return u"PropertyList-Property link between {0} - {1}".format(
            self.property_list, self.property)


class Property(models.Model):

    __metaclass__ = TransMeta

    class Meta:
        translate = ('local_name', 'local_comment')
        verbose_name_plural = "Properties"
        ordering = ["name"]

    name = models.SlugField(max_length=32, unique=True)
    local_name = models.CharField(blank=True, max_length=255,
                                  verbose_name=_("Localized name"))
    local_comment = models.TextField(
        blank=True, verbose_name=_("Localized comment"))
    quantity = models.ForeignKey('Quantity')

    def __unicode__(self):
        return self.name

    @classmethod
    def get_name_to_prop_map(cls):
        res = {}
        for prop in cls.objects.all():
            res[prop.name] = prop
        return res


class TypedModel(models.Model):
    """
    An abstract parent for models with type info, it defines common constants
    and not fields
    """
    class Meta:
        abstract = True

    TYPE_INT = 1
    TYPE_FLOAT = 2
    TYPE_TEXT = 3
    TYPE_IP_ADDR = 4
    TYPE_URL = 5
    TYPE_LOCAL_TEXT = 6
    TYPE_BOOL = 7
    TYPE_DATETIME = 8

    TYPE_CHOICES = ((TYPE_INT, 'int'),
                    (TYPE_FLOAT, 'float'),
                    (TYPE_TEXT, 'text'),
                    (TYPE_IP_ADDR, 'ip_addr'),
                    (TYPE_URL, 'url'),
                    (TYPE_LOCAL_TEXT, 'local_text'),
                    (TYPE_BOOL, 'bool'),
                    (TYPE_DATETIME, 'datetime')
                    )

    TYPE_NAME_TO_ID = dict([(v, k) for k, v in TYPE_CHOICES])
    FIELD_MAPPING = dict(TYPE_CHOICES)


class Quantity(TypedModel):

    class Meta:
        verbose_name_plural = "Quantities"
        ordering = ["name"]

    name = models.SlugField(max_length=32, unique=True)
    unit = models.CharField(max_length=32, null=True)
    value_type = models.IntegerField(choices=TypedModel.TYPE_CHOICES)

    @classmethod
    def type_name_to_int(cls, name):
        return cls.TYPE_NAME_TO_ID.get(name)

    def __unicode__(self):
        return self.name

    def get_type_name(self):
        return self.FIELD_MAPPING.get(self.value_type)


#
# Following models describe a concrete data that correspond to an abstract model
# described by the classes above
#

class PropertyValueCollection(models.Model):
    """
    Represents a collection of concrete property values.
    It is meant to describe one object in real world with different values
    for a set of properties.
    It is intended to me used in one-to-many of one-to-one relationship
    in other applications. In here, it is more of an abstract object model
    """
    # it is just a recipient of PropertyValue foreign key, so it does
    # not have any other attributes outside the implied primary key

    def __unicode__(self):
        return u"PropertyValueCollection pk={0}".format(self.pk)

    def fill_in_property_list(self, prop_list):
        """
        Take a description of a set of properties from a PropertyList instance
        and fill the corresponding values there

        Args:
            prop_list (PropertyList): property list to be filled in

        Side effects:
            Adds and populates :py:attr:property_values attr and
            :py:attr:bound_value to prop_list and its children
        """
        prop_list.retrieve_child_structure(retrieve_properties=True)
        # dumb implementation - retrieve all values in this collection
        # and then use only the ones that are referenced in prop_list structure
        prop_id_to_value = self.create_prop_id_to_value_dict()

        def fill_it(pset):
            pset.property_values = []
            if pset.bound_property:
                pset.bound_value = prop_id_to_value.get(pset.bound_value.pk)
            for prop in pset.property_objects:
                # we assume that is has property_objects - retrieve_properties
                # was run on it
                value = prop_id_to_value.get(prop.pk)
                pset.property_values.append(value)
            for child in pset.child_objects:
                fill_it(child)
        fill_it(prop_list)

    def flatten_property_list(self, prop_list, pruning=True):
        """
        Return a table like structure filled with data from this collection and
        described by the PropertyList prop_list.

        Args:
            prop_list (PropertyList): property list to be filled in
            pruning (bool):  removes empty nodes if enabled

        Returns:
            [property|propertlyList, is_parent, level, propval],
            ...]

        Side effects:
            runs :py:method:retrieve_child_structure on the prop_list
        """
        prop_list.retrieve_child_structure(retrieve_properties=True)
        # dumb implementation - retrieve all values in this collection
        # and then use only the ones that are referenced in prop_list structure
        prop_name_to_value = self.create_prop_name_to_propval_dict()
        result = []

        def fill_it(pset, level):
            # put self into the list
            if pset.bound_property:
                val = prop_name_to_value.get(pset.bound_property.name)
                parent = True
            elif pset.collapsible:
                # we inline all True values
                val = []
                for prop in pset.property_objects:
                    propval = prop_name_to_value.get(prop.name)
                    if propval.get_value() == True:
                        val.append(propval)
                parent = False
            else:
                val = None
                parent = True
            result.append([pset, parent, level, val])  # True means parent
            for child in pset.child_objects:
                fill_it(child, level + 1)
            if not pset.collapsible:
                for prop in pset.property_objects:
                    # we assume that is has property_objects -
                    # retrieve_properties was run on it
                    if prop.name in prop_name_to_value:
                        propval = prop_name_to_value.get(prop.name)
                        result.append([prop, False, level + 1, propval])

        def prune_empty_nodes(prop_list):
            """Recursively walk the tree of prop_list and delete empty nodes.

            :returns:  bool -- is the node empty (no properties and childs).
            """
            empty_leaves = []
            for i, child in enumerate(prop_list.child_objects):
                empty_leaf = prune_empty_nodes(child)
                if empty_leaf:
                    # we can't delete the leaf here, just remember it
                    empty_leaves.append(i)

            # delete the remembered empty leaves
            for empty_idx in sorted(empty_leaves, reverse=True):
                del prop_list.child_objects[empty_idx]

            no_childs = len(prop_list.child_objects) == 0
            empty_props = not any(prop_name_to_value.get(x.name)
                                  for x in prop_list.property_objects)
            if no_childs and empty_props:
                return True
            return False

        if pruning:
            prune_empty_nodes(prop_list)
        fill_it(prop_list, 0)
        return result

    def create_prop_id_to_value_dict(self):
        res = {}
        for propval in self.propertyvalue_set.all():
            res[propval.property.id] = propval.get_value()
        return res

    def create_prop_name_to_value_dict(self):
        res = {}
        for propval in self.propertyvalue_set.select_related().all():
            res[propval.property.name] = propval.get_value()
        return res

    def create_prop_name_to_propval_dict(self):
        res = {}
        for propval in self.propertyvalue_set.select_related().all():
            res[propval.property.name] = propval
        return res

    def deep_delete(self):
        self.delete()


class TypedValue(TypedModel):
    """
    Represents one value of selected type. The type is represented
    by the :py:attr:value_type. The value itself should be extracted and set
    using the :py:meth:get_value and :py:meth:set_value methods.
    """
    __metaclass__ = TransMeta

    class Meta:
        abstract = True
        translate = ('val_local_text',)

    # fields are defined here
    value_type = models.IntegerField(choices=TypedModel.TYPE_CHOICES)
    # the following fields represent the possible types this model can store
    # only one of the values should be present at any time and it should
    # be reflected in the value_type attribute
    val_int = models.IntegerField(null=True)
    val_float = models.FloatField(null=True)
    val_text = models.TextField(null=True)
    val_ip_addr = models.GenericIPAddressField(null=True)
    val_url = models.URLField(null=True)
    val_bool = models.NullBooleanField()
    val_local_text = models.TextField(null=True,
                                      verbose_name=_("Localized text"))
    val_datetime = models.DateTimeField(null=True)

    def __unicode__(self):
        return unicode(self.get_value())

    def is_localized(self):
        if self.value_type == self.TYPE_LOCAL_TEXT:
            return True
        return False

    def _get_active_attr_name(self, lang=None):
        field_name = self.FIELD_MAPPING.get(self.value_type)
        if not field_name:
            raise Exception("Unknown type %s", self.value_type)
        if self.is_localized():
            # need language first
            if lang is None:
                lang = get_language()
            lang = lang.split("-")[0]  # remove sublanguage part
            field_name += "_" + lang
        return "val_" + field_name

    def get_value(self, lang=None):
        attr_name = self._get_active_attr_name()
        value = getattr(self, attr_name)
        return value

    def set_value(self, value, lang=None):
        attr_name = self._get_active_attr_name(lang=lang)
        setattr(self, attr_name, value)

    def validate(self):
        """
        Return True if the instance is correctly populated with data
        - there is only one non empty val_xxx field and it corresponds to
        :py:attr:value_type attr
        """
        field_name = self.FIELD_MAPPING.get(self.value_type)
        if not field_name:
            return False
        for type_desc in self.TYPE_CHOICES:
            if type_desc[1] != field_name:
                # this field should be null
                attr_name = "val_" + type_desc[1]
                if hasattr(self, attr_name) and \
                   getattr(self, attr_name) != None:
                    return False
        return True


class PropertyValue(TypedValue):
    """
    Represents a specific value for one property (with a optional note)
    """
    __metaclass__ = TransMeta

    class Meta:
        translate = ('note',)

    property = models.ForeignKey('Property')
    collection = models.ForeignKey('PropertyValueCollection')
    note = models.TextField(blank=True, verbose_name=_("Note"))

    def __unicode__(self):
        if self.property.quantity.unit:
            return u"{0} = {1} {2}".format(self.property, self.get_value(),
                                           self.property.quantity.unit)
        else:
            return u"{0} = {1}".format(self.property, self.get_value())
