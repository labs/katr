from django.db import models
from django.utils.translation import ugettext_lazy as _

# third party libs
from transmeta import TransMeta


class StaticText(models.Model):
    __metaclass__ = TransMeta

    class Meta:
        translate = ('text',)
        ordering = ['name']

    name = models.SlugField()
    text = models.TextField(verbose_name=_("Text"), blank=True)

    def __unicode__(self):
        return self.name
