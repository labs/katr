function initComparison () {
    jQuery('.compare-link').click(clickHandler);
}
function clickHandler (e) {
    e.preventDefault();
    var anchor = jQuery(this);
    var url = anchor.attr('href');
    jQuery.get(url, function(data) {
        // update all the comparison links with same URL as the current one
        jQuery('.compare-link[href="'+url+'"]').each(function(){
            var box = jQuery(this).parent().parent();
            box.html(data.html);
        });
        // show/hide comparison link in header if it's meaningful
        if (data.selected_reviews_cnt > 1) {
            jQuery("#comparison-button").css('display', 'inline-block');
            var newValue = jQuery("#comparison-button a").html().replace(/\d+/g, data.selected_reviews_cnt);
            jQuery("#comparison-button a").html(newValue);
        } else {
            jQuery("#comparison-button").hide();
        }
    }).success( function (e) {
        // handle new elements identically
        jQuery('.compare-link[href*="'+e.slug+'"]').click(clickHandler);
    });
}
jQuery(document).ready(function() {
    initComparison();
});
