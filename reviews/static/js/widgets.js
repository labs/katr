var ipv6testURIs = [
    "//ipv4.test-ipv6.cz/ip/?callback=?",
    "//ipv6.test-ipv6.cz/ip/?callback=?",
//    "//[2001:1488:800:400::2:120]:80/ip/?callback=?",
    "//ipv6.test-ipv6.cz/ip/?callback=?&size=",
    "//ds.test-ipv6.cz/ip/?callback=?"
]
function runDnssecTest() {
    var url = "//www.rhybar.cz/";
    successCallback = function (data) {
        // Not DNSSEC protected
        jQuery('#dnssec-widget .unknown').hide();
        jQuery('#dnssec-widget .error').show();
    }
    errorCallback = function (e) {
        // DNSSEC protected
        jQuery('#dnssec-widget .unknown').hide();
        jQuery('#dnssec-widget .success').show();
    }
    runTest(url, successCallback, errorCallback);
}
function runIpv6Test() {
    var numTests = ipv6testURIs.length;
    var successfulTests = 0;
    var finishedTests = 0;
    var activeRequests = 0;
    ready = true;
    successCallback = function (data) {
        successfulTests++;
        finishedTests++;
        // jQuery('#ipv6-widget .result-status').html(successfulTests + '/' + finishedTests);
        if (--activeRequests == 0) {
            updateIpv6Result(ready);
        }
    }
    errorCallback = function (e) {
        finishedTests++;
        if (ready) {
            ready = false;
        }
        if (--activeRequests == 0) {
            updateIpv6Result(ready);
        }
    }
    for (var i=0; i<ipv6testURIs.length; i++) {
        var url = ipv6testURIs[i];
        activeRequests++;
        runTest(url, successCallback, errorCallback);
    }
}
function runTest(url, successCallback, errorCallback) {
    jQuery.jsonp({
        "url": url,
        "cache": false,
        "pageCache": false,
        "timeout": 30000,
        "success": successCallback,
        "error": errorCallback
    });
}
function updateIpv6Result(ready) {
    jQuery('#ipv6-widget .unknown').hide();
    if (ready) {
        jQuery('#ipv6-widget .success').show();
    } else {
        jQuery('#ipv6-widget .error').show();
    }
}
