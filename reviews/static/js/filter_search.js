"use strict";
/*jsl:option explicit*/

function katrSearch() {
    this.reviews = null;
    this.properties = null;
    this.true_value = "Yes";
    this.sortBy = null;
    this.sortDesc = false;
}
function compareNumbers(a, b) {
    return a-b;
}
function compareStrings(a, b) {
    return a.toString().localeCompare(b.toString());
}

katrSearch.prototype.setTrueValue = function(value) {
    this.true_value = value;
};

katrSearch.prototype.init = function(filter_type, reviews, properties, default_order) {
    this.reviews = reviews;
    this.properties = properties;
    this.default_order = default_order;
    this.selected_reviews = reviews;
    this.filter_type = filter_type;
    this.active_filter_id = '#filter-params-' + filter_type;
    this.slider_quantities = ['data_speed', 'weight', 'length', 'power'];
    /* a mapping between input id and review slug */
    this.inclusion_list = new Object();
    /* monitors which inputs belong together under which property */
    this.input_groups = new Object();
    this.slider_widgets = new Object();
    this.init_table();
    this.update_review_list();
};

katrSearch.prototype.init_table = function() {
    for (var prop in this.properties) {
        var possible_vals = new Array();
        var possible_vals_dict = new Object();
        for (var review_slug in this.reviews) {
            var review = this.reviews[review_slug];
            var val = review.properties[prop];
            if (val) {
                if (!possible_vals_dict.hasOwnProperty(val)) {
                    possible_vals_dict[val] = 1;
                    possible_vals.push(val);
                } else {
                    possible_vals_dict[val] += 1;
                }
            }
        }
        //console.log(prop, possible_vals, possible_vals.length);
        var prop_obj = this.properties[prop];
        prop_obj.slider = false;
        if (possible_vals.length == 1 && !(prop=='manufacturer')) {
            var val = possible_vals[0];
            jQuery(this.active_filter_id+" #td_"+prop).html(val);
            jQuery(this.active_filter_id+" #td_"+prop).append("<span class='count'>["+
                                    possible_vals_dict[val]+"]</span>");
        } else if (this.useSlider(prop, possible_vals.length)) {
            /* let's use slider widget to filter items based on range */
            var slider_widget = Object();
            var el_id = "slider-" + prop;
            slider_widget.el_id = el_id;
            prep_html = jQuery(
                    "<span id='min-" + el_id + "' class='slider-value'></span>" +
                    "<div id='" + el_id + "'></div>" +
                    "<span id='max-" + el_id + "' class='slider-value'></span>"
            );
            possible_vals.sort(compareNumbers);
            var min = possible_vals[0];
            var max = possible_vals[possible_vals.length-1];
            slider_widget.min = min;
            slider_widget.max = max;
            jQuery(this.active_filter_id+" #td_"+prop).html(prep_html);
            jQuery("#" + el_id).slider({
                range: true,
                min: min,
                max: max,
                values: [min, max],
                slide: function( event, ui ) {
                    jQuery('#min-' + this.id).html(ui.values[0]);
                    jQuery('#max-' + this.id).html(ui.values[1]);
                }
            }).on("slidechange", this.update_review_list.bind(this));
            this.slider_widgets[prop] = slider_widget;
            jQuery("#min-" + el_id).html(min);
            jQuery("#max-" + el_id).html(max);
        } else if (possible_vals.length <= 6 || prop=='manufacturer' ) {
                /* let's use a simple selection from possible values */
                var input_group = new Object();
                possible_vals.sort(compareStrings);
                var el_id = "";
                for (var j=0; j<possible_vals.length; j++) {
                    var val = possible_vals[j];
                    el_id = "check_"+prop+"_val_"+val;
                    // replace spaces and commas, so it doesn't confuse jQuery selectors
                    el_id = el_id.replace(/(\s|,)+/g, '-');
                    if (prop=='manufacturer') {
                        var prep_html = jQuery("<input type='checkbox' id='"+el_id+"'>"+
                                               "<label for='"+el_id+"'>"+val+"<span></span></label>").
                                              change(this.update_review_list.bind(this));
                    } else {
                        var prep_html = jQuery("<input type='checkbox' id='"+el_id+"'>"+
                                               "<label for='"+el_id+"'>"+val+"</label>").
                                              change(this.update_review_list.bind(this));
                    }
                    this.inclusion_list[el_id] = new Object();
                    /* populate the inclusion list */
                    for (var review_slug in this.reviews) {
                        var review = this.reviews[review_slug];
                        var rev_val = review.properties[prop];
                        if (rev_val == val)
                            this.inclusion_list[el_id][review_slug] = null;
                    }
                    input_group[el_id] = null;
                    jQuery(this.active_filter_id+" #td_"+prop+",#manufacturers-filter #td_"+prop)
                        .append(prep_html);
                    if (prop != 'manufacturer') {
                        jQuery(this.active_filter_id+" #td_"+prop).append("<span class='count' id='count_"+el_id+
                                "'>["+possible_vals_dict[val]+"]</span>");
                    }
                }
                this.input_groups[prop] = input_group;
        } else {
            /* we need some range widget or something like this for non-numeric types */
            jQuery(this.active_filter_id+" #td_"+prop).html("Too many values "+possible_vals.length);
        }
    }
    //console.log(this.inclusion_list);
};

katrSearch.prototype.update_review_list = function() {
    /* select matching reviews for all input groups */
    var subselects = new Object();
    for (var prop_name in this.input_groups) {
        var subsel = new Object();
        for (var el_id in this.input_groups[prop_name]) {
            if (jQuery('#'+el_id).prop('checked')) {
                /* select all that are on the inclusion list of this one */
                for (var rev_slug in this.inclusion_list[el_id])
                    subsel[rev_slug] = null;
            }
        }
        subselects[prop_name] = subsel;
    }
    /* add matching reviews for all slider groups */
    for (var prop_name in this.slider_widgets) {
        var subsel = new Object();
        var slider_widget = this.slider_widgets[prop_name];
        var vals = jQuery('#' + slider_widget.el_id).slider('values');
        /* if at least one current slider limit is different from default */
        if (vals[0] > slider_widget.min || vals[1] < slider_widget.max) {
            /* add empty review id to suppress
             * "show all if no review match" behavior
             */
            subsel['null_review'] = null;
            for (var review_slug in this.reviews) {
                var rev_val = this.reviews[review_slug].properties[prop_name];
                if (rev_val >= vals[0] && rev_val <= vals[1]) {
                    subsel[review_slug] = null;
                }
            }
        }
        subselects[prop_name] = subsel;
    }
    /* select reviews that match all subselects */
    var selected = this.get_matching_reviews(subselects);
    var sel_count = 0;
    for (var review_slug in this.reviews) {
        if (selected.hasOwnProperty(review_slug)) {
            jQuery(".rev_"+review_slug).show();
            sel_count = sel_count + 1;
        } else {
            jQuery(".rev_"+review_slug).hide();
        }
    }
    if (sel_count) {
        jQuery('#no-matching-review').hide();
        jQuery('#sorting-header .sorting-button').show();
    } else {
        jQuery('#no-matching-review').show();
        jQuery('#sorting-header .sorting-button').hide();
    }
    /* now update the counts, we reuse some data here */
    var subsel_copy = subselects;
    for (var prop_name in subselects) {
        var backup_subsel = subsel_copy[prop_name];
        delete subsel_copy[prop_name];
        /* matching for any value for prop_name but limited by all other
         * selections */
        selected = this.get_matching_reviews(subsel_copy);
        var possible_vals_dict = new Object();
        for (var review_slug in selected) {
            var review = this.reviews[review_slug];
            var key = "check_"+prop_name+"_val_"+review.properties[prop_name];
            key = key.replace(/(\s|,)+/g, '-');
            if (!possible_vals_dict.hasOwnProperty(key)) {
                possible_vals_dict[key] = 1;
            } else {
                possible_vals_dict[key] += 1;
            }
        }
        for (var el_id in this.input_groups[prop_name]) {
            if (possible_vals_dict.hasOwnProperty(el_id)) {
                if (prop_name!='manufacturer') {
                    jQuery("#"+el_id).attr("disabled", false);
                    jQuery("#count_"+el_id).html("["+possible_vals_dict[el_id]+"]");
                }
            } else {
                if (prop_name!='manufacturer') {
                    jQuery("#"+el_id).attr("disabled", true);
                    jQuery("#count_"+el_id).html("[0]");
                }
            }
        }
        subsel_copy[prop_name] = backup_subsel;
    }
};

katrSearch.prototype.get_matching_reviews = function(select_groups) {
    /* now select those that are on all subsels */
    var selected = new Object();
    for (var review_slug in this.reviews) {
        var is_ok = true;
        for (var prop_name in select_groups) {
            var subsel = select_groups[prop_name];
            if (jQuery.isEmptyObject(subsel) ||
                    subsel.hasOwnProperty(review_slug)) {
                /* this is ok */
            } else {
                is_ok = false;
                break;
            }
        }
        if (is_ok)
            selected[review_slug] = null;
    }
    return selected
}

katrSearch.prototype.reload = function() {
    jQuery('*[id^="td_"]').empty();
    this.init(this.filter_type, this.reviews, this.properties, this.default_order);
}

katrSearch.prototype.clean_manufacturers = function() {
    jQuery('#td_manufacturer input').each( function () {
        var input = jQuery(this);
        if (input.prop('checked')) {
            input.click();
        }
    })
}

katrSearch.prototype.sort_comparator = function(a, b) {
    var result = 0;
    if (this.sortDesc)
        result = a.sort_cols[this.sortBy] < b.sort_cols[this.sortBy] ? 1 : -1;
    else
        result = a.sort_cols[this.sortBy] > b.sort_cols[this.sortBy] ? 1 : -1;
    if (typeof a.sort_cols[this.sortBy] == "boolean")
        result = -result;
    return result;
};

katrSearch.prototype.default_sort = function() {
    if (this.sortBy==null) {
        return
    } else {
        this.sortBy = null;
        this.sortDesc = false;
        var reviewTable = jQuery(".part-review-table");
        for (var i in this.default_order) {
            jQuery(".rev_"+ this.default_order[i] + ":visible").appendTo(reviewTable);
        }
    }
}

katrSearch.prototype.sort_review_list = function(sort_by) {
    if (this.sortBy == sort_by) {
        this.sortDesc = !this.sortDesc;
    }
    else {
        this.sortDesc = false;
    }
    this.sortBy = sort_by;

    var sortedReviews = [];
    for (var review_slug in this.reviews) {
        if (!this.reviews.hasOwnProperty(review_slug))
            continue;
        sortedReviews.push({
            review_slug: review_slug,
            sort_cols: this.reviews[review_slug].sort_cols});
    }

    /*
    * workaround - Function.prototype.bind is not supported in IE < 9,
    * alternatively a compatibility script exists for older browsers
    */
    var self = this;
    sortedReviews.sort(function(a, b){return self.sort_comparator(a, b);});

    var unknownValues = [];
    var reviewTable = jQuery(".part-review-table");
    for (var i in sortedReviews) {
        if (!sortedReviews.hasOwnProperty(i))
            continue;
        if (sortedReviews[i].sort_cols[this.sortBy] == null)
            unknownValues.push(sortedReviews[i]);
        else
            jQuery(".rev_"+ sortedReviews[i].review_slug + ":visible").appendTo(reviewTable);
    }

    for (i in unknownValues) {
        if (!unknownValues.hasOwnProperty(i))
            continue;
        jQuery(".rev_"+ unknownValues[i].review_slug + ":visible").appendTo(reviewTable);
    }
};

katrSearch.prototype.refresh = function(filter_type, reviews, properties, default_order) {
    if (reviews && properties && default_order) {
        this.reviews = reviews;
        this.properties = properties;
        this.default_order = default_order;
    }
    this.filter_type = filter_type;
    var selected_inputs = []
    /* find currently selected inputs, ignore sliders */
    jQuery('*[id^="td_"] > input:checked').each( function(index) {
        selected_inputs.push(jQuery(this).attr('id'));
    });
    this.reload();
    /* select previously selected inputs, that are still present, not sliders */
    for (var i in selected_inputs) {
        jQuery(this.active_filter_id+' #'+selected_inputs[i]+', #manufacturers-filter #'+selected_inputs[i]).click();
    }
};
katrSearch.prototype.useSlider = function(property, min_vals_length) {
    /* returns true for numeric quantity types in this.slider_quantities */
    var quantity = this.properties[property].quantity
    return min_vals_length > 2 && this.slider_quantities.indexOf(quantity) >= 0;
}

