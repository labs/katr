# built-in modules
from os import listdir
from os.path import join

# local imports
from .models import RouterReview
from routers.models import Router


def update():
    reviews = RouterReview.objects.all()
    for review in reviews:
        review.update_extra_fields()


def add_pictures(picture_dir, clear=False):
    pict_files = sorted(listdir(picture_dir))
    for router in Router.objects.all():
        if clear:
            router.routerpicture_set.all().delete()
        for filename in pict_files:
            if router.slug.lower() in filename.lower():
                router.add_local_picture(join(picture_dir, filename))
