��    q      �  �   ,      �	  i   �	  �   �	  A   �
  6   �
  >     b   N  D   �  �   �  +   �     �     �     �     �  <     	   D     N     V     b     n          �  
   �     �     �     �     �     �     �     �                    *  
   ;     F     [     l     �     �     �  D   �  )   �       .   $     S     Y     `     m     u     �     �     �  	   �     �     �     �  �   �     X     a     j  
   p     {     �     �     �     �     �     �  &   �  H     I   O  &   �     �     �     �     �     �     �  #        ,      A  2   b     �     �     �     �     �  
   �  '   �  3     F   5     |     �  
   �  	   �     �     �  	   �     �  #   �             
   !     ,     0     7     S     m     r     �     �     �  x  �  h   .  �   �  h   H  7   �  A   �  f   +  B   �  �   �  -   �  
   �     �     �     �  G   �  
   G     R     c     v     �     �     �     �     �     �     �     �     �               9     ?     F     T     f     |     �     �     �     �     �  M   �  A   ;     }  <   �     �  	   �     �     �     �     �          &     )     8     G     Y  �   f     �                         +     H     ]     o     �     �  *   �  d   �  S   -  +   �     �     �     �     �     �     �  *         3   "   J   J   m      �      �      �      �   	   �      �   +   �   /   !  Y   M!     �!     �!  
   �!  	   �!     �!     �!     "     "     1"     N"     _"  	   y"     �"     �"     �"     �"     �"     �"     �"     �"     �"     b   ]   d   @       4          &   $      8       \   S   C   A   q                  )          G      D       H   :   T   F   (           R   5   9   Z       I   W              *      _       +       i       P           j      o       3   c   g   %          .      p       ^   	   J              X   6   K   [      U   
   M   Y   =       ;         B   f       L      ?   n      !   2       "                 `   Q   1   <   e      ,       h   V   #          O   -      a   >       E             '           /          l   k                        0      7   m       N    
                            Comparison of %(selected_review_count)s routers
                             
                        Send us an email on <a href="mailto:katr@labs.nic.cz">katr@labs.nic.cz</a>
                        or use this form:
                     
        Comparison of %(selected_review_count)s routers
         
        Number of matching reviews: %(revcount)s
     
        We were not able to find expression "%(query)s".
     
        You are seeing this info because you are logged in as staff user '%(username)s'.
         
    The review is available <a href="%(review_url)s">here</a>.
     
On this page you can upload one review in CSV format. If you upload a review
for an already existing combination of model, firmware and date, it will be
replaced instead.
 
You are logged in as <b>%(username)s</b>.
 About Account is disabled Add to comparison All routers All routers on this web have been tested in the CZ.NIC Labs. Backstage Bargain CZ.NIC Labs CZ.NIC Seal Cancel selection Clean filter Compare Comparison Complete list Contact supplier Detail Detailed results Django admin interface Email Enter valid email address Error Error: Exception type: Exception value: Extra info Filter by parameters Firmware version Hide complete filter Hide equal values Homepage IPv6 support Independent reviews of routers with focus on new technology support. Invalid login: wrong username or password Let us know List of all routers tested in the CZ.NIC Labs. Login Logout Manufacturer Message Methodology Mistake? Money saving IPv6 No Not ready Not secured Number of LAN ports Other sites Our admins were notified about the problem and will
        attend to it as soon as possible. Please try this link later.
         Page top Password Price Questions? Ready Remove from comparison Review debug page Review file Router catalog Router comparison Routers Routers combined with ADSL/VDSL modem. Routers that achieved at least 175 points in our test ordered by points. Routers with IPv6 support and complete IPv6 speed tests ordered by price. Routers with the best value for money. Score Seal of the CZ.NIC Labs Search results Secured Send Server error. Show comparison of selected routers Show complete filter Some tests are not complete yet. Sorry, the page you are looking for was not found. Source Source: Manufacturers webpage Summary Text Thanks The review The review is <b>not published</b> yet. There are no matching reviews for given parameters. This chart can't be loaded. We'll try to fix this as soon as possible. This field is required Top routers Traceback: USB ports Upload Upload review User page Username We apologize for the inconvenience. What is DNSSEC? Why IPv6 router? With modem Yes Yes,No Your DNSSEC protection test Your IPv6 connection test from min-priceUnknown points search querySearch test-progressUnknown Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-18 11:37+0100
PO-Revision-Date: 2012-09-18 13:47+0200
Last-Translator: Beda Kosata <bedrich.kosata@nic.cz>
Language-Team: Czech <>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 
                            Porovnání %(selected_review_count)s routerů
                             
                        Napište nám na <a href="mailto:katr@labs.nic.cz">katr@labs.nic.cz</a>
                        a nebo využijte tento formulář:
                     
                            Porovnání %(selected_review_count)s routerů
                             
        Počet nalezených recenzí: %(revcount)s
     
    Bohužel se nám nepodařilo výraz "%(query)s" najít.
     
        Tyto informace vidíte, protože jste zalogován jako interní uživatel '%(username)s'.
	    
    Recenze je k dispozici <a href="%(review_url)s">zde</a>.
     
Na této stránce můžete nahrát recenzi v CSV formátu. Pokud nahrajete recenzi
pro již existující kombinaci modelu, firmwaru a data, dojde k přepsání recenze.
 
Jste přihlášen jako <b>%(username)s</b>.
 O projektu Účet je deaktivovaný Přidat router k porovnání Všechny routery Všechny routery na tomto webu byly testovány v Laboratořích CZ.NIC. Zákulisí Výhodná koupě Laboratoře CZ.NIC Pečeť CZ.NIC Zrušit výběr Vymazat filtr Porovnej Porovnání Celý seznam Cenu zjistíte u dodavatele Detail Podrobné výsledky Django admin rozhraní Email Zadejte, prosím, platný email Chyba Chyba: Typ výjimky: Hodnota výjimky: Dodatečné informace Filtr podle parametrů Verze firmware Skrýt úplný filtr Skrýt stejné hodnoty Úvodní stránka Podpora IPv6 Nezávislé recenze routerů se zaměřením na podporu nových technologií. Neplatné přihlášení: špatné uživatelské jméno či heslo Napište nám Seznam všech routerů testovaných v Laboratořích CZ.NIC. Přihlášení Odhlásit Výrobce Text dotazu Metodika Našli jste chybu? IPv6 za dobrou cenu Ne Nepodporováno Nezabezpečeno Počet LAN portů Další weby Naši administrátoři byli upozorněni na problém a budou se 
mu věnovat, jakmile to bude možné. Prosím, zkuste tento odkaz později.
	 Začátek stránky Heslo Cena Máte dotaz? V pořádku Odebrat router z porovnání Debugování recenze Soubor s recenzí Katalog routerů Porovnání routerů Routery Routery s integrovaným ADSL/VDSL modemem. Routery, které v našem testu získaly alespoň 175 bodů, řazené podle počtu získaných bodů. Routery s podporou IPv6 a s kompletními testy rychlosti IPv6, řazené podle ceny. Routery řazené podle poměru cena/výkon. Skóre Pečeť Laboratoří CZ.NIC Výsledky hledání Zabezpečeno Odeslat Chyba serveru. Přejít k porovnání vybraných routerů Zobrazit úplný filtr Některé testy nejsou kompletní. Je nám líto, ale stránku, kterou hledáte, se nám nepodařilo nalézt. Zdroj Zdroj: Web výrobce Resumé Text Děkujeme Recenze Recenze <b>nebyla</b> ještě publikována. Žádný router neodpovídá zvolenému filtru. Tento žebříček se nepodařilo načíst. Pokusíme se to v nejbližší době opravit. Vyplňte, prosím, toto pole Špičkové routery Traceback: USB porty Nahrát Nahrát recenzi Uživatelská stránka Uživatelské jméno Omlouváme se za komplikace. Co je to DNSSEC? Proč router s&nbsp;IPv6? S modemem Ano Ano,Ne Test vaší ochrany DNSSEC Test vašeho IPv6 připojení od Nezjištěna bodů Vyhledat Nezjištěno 