from django import forms
from django.utils.translation import ugettext_lazy as _


class ReviewUploadForm(forms.Form):
    review_file = forms.FileField(label=_("Review file"))


class QuestionForm(forms.Form):
    message = forms.CharField(label=_("Message"), required=True,
                              widget=forms.Textarea(attrs={'rows': '6', 'cols': '30',
                                                           'class': 'required',
                                                           'placeholder': _("Message")}))
    email = forms.EmailField(label=_("Email"), required=True,
                             widget=forms.TextInput(attrs={'class': 'required email',
                                                           'size': '30',
                                                           'placeholder': _("Email")}))
