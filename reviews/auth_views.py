# Django imports
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login, logout
from django.views.generic import TemplateView, View

# Local imports
from .mixins import BaseCacheControlMixin, LoginRequiredMixin


class LoginView(BaseCacheControlMixin, TemplateView):
    template_name = 'backstage/login.html'

    def get(self, request, *args, **kwargs):
        next_page = request.GET.get("next", reverse('user_page'))
        if request.user and request.user.is_active:
            return HttpResponseRedirect(next_page)
        else:
            return self.render_to_response(dict(next=next_page))

    def post(self, request, *args, **kwargs):
        next_page = request.POST.get("next", reverse('user_page'))
        username = request.POST.get('username')
        password = request.POST.get('password')
        error = ""
        if username and password:
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(next_page)
                else:
                    error = _("Account is disabled")
            else:
                error = _("Invalid login: wrong username or password")
        return self.render_to_response(dict(error=error, next=next_page))


class LogoutView(LoginRequiredMixin, BaseCacheControlMixin, View):

    def get(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(reverse('login_page'))
