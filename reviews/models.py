# Django imports
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

# local imports
from routers.models import RouterFirmware
from properties.models import PropertyValueCollection

# third party libs
from transmeta import TransMeta
from autoslug import AutoSlugField
import requests


class RouterReview(models.Model):
    __metaclass__ = TransMeta

    slugger = lambda x: unicode(x.router_firmware) + "-" + x.date.isoformat()

    class Meta:
        translate = ('summary', 'text')
        ordering = ['slug']

    router_firmware = models.ForeignKey(RouterFirmware)
    properties = models.OneToOneField(PropertyValueCollection)
    date = models.DateField()
    summary = models.TextField(verbose_name=_("Summary"), blank=True)
    text = models.TextField(verbose_name=_("Text"), blank=True)
    author = models.ForeignKey('Author')
    slug = AutoSlugField(populate_from=slugger, unique=True, max_length=100)
    published = models.BooleanField(default=True)
    heureka_id = models.IntegerField(null=True, blank=True)
    stock_link = models.URLField(null=True, blank=True)
    score = models.PositiveIntegerField(null=True, blank=True)
    min_price = models.PositiveIntegerField(null=True, blank=True)
    bargain = models.FloatField(null=True, blank=True)
    # extra fields for property_table list
    has_usb = models.BooleanField(default=False)
    lan_ports = models.PositiveIntegerField(default=1)
    ipv6_support = models.BooleanField(default=True)
    ipv6_speed = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        return u"Review of {0} ({1} by {2})".format(self.router_firmware,
                                                    self.date,
                                                    self.author)

    def short_name(self):
        return u"{0}, {1}".format(self.router_firmware.router,
                                  self.router_firmware.name)

    def delete(self):
        self.properties.delete()
        super(RouterReview, self).delete()

    def update_extra_fields(self, init=False):
        self.score = self.compute_score()
        self.min_price = self.download_price()
        self.bargain = self.compute_bargain()
        prop_dict = self.properties.create_prop_name_to_value_dict()
        self.has_usb = prop_dict.get('usb_port_number', 0) > 0
        self.lan_ports = prop_dict.get('lan_port_number', 0)
        self.ipv6_support = prop_dict.get('ipv6_support', False)
        self.ipv6_speed = prop_dict.get('wan_lan_v6_iperf', 0)
        if init:
            pass
        self.save()

    def download_price(self):
        if self.heureka_id:
            min_price = self._zbozi_price(self.heureka_id)
            if min_price:
                return min_price
        else:
            return self.min_price

    def compute_score(self):
        prop_name_to_value_dict = self.properties.create_prop_name_to_value_dict()
        score = 0
        max_score = 250  # 450 including speed_score
        score += self._ipv6_score(prop_name_to_value_dict)
        score += self._functions_score(prop_name_to_value_dict)
        # speed_score = self._speed_score(prop_name_to_value_dict)
        # if speed_score == 0:
        #     return 0
        # else:
        #     score += speed_score
        return score

    def compute_bargain(self):
        if self.min_price > 0 and self.score:
            return float(self.score) / float(self.min_price)
        else:
            return None

    def get_stock_link(self, stock_id):
        link = ''
        try:
            content = self._stock_id_request_content(stock_id)
            if content:
                product_name = content['name']
                # url from the first (probably most relevant) result
                name_request_content = self._stock_name_request_content(
                    product_name)
                if len(name_request_content):
                    link = name_request_content[0]['url']
                else:
                    print 'Link not found. No results found for name: %s' % product_name
        except Exception:
            print 'Link not found.'
            return None
        else:
            return link

    def _zbozi_price(self, stock_id):
        try:
            content = self._stock_id_request_content(stock_id)
            min_price = round(content['minPrice'] / 100.0)
            if not min_price:
                return None
        except Exception:
            return None
        else:
            return min_price

    def _stock_id_request_content(self, stock_id):
        '''
        Get JSON like content of request on stock API based on product ID.
        '''
        try:
            url = 'http://api.zbozi.cz/1/products/%s.json' % stock_id
            r = requests.get(url, auth=(
                settings.STOCK_USERNAME, settings.STOCK_PASSWORD))
            if r.status_code in [404, 500]:
                print "Product with ID %s not found on zbozi.cz" % stock_id
                return None
        except requests.exceptions.RequestException:
            return None
        else:
            return r.json()

    def _stock_name_request_content(self, product_name):
        '''
        Get JSON like content of request on stock API based on product name.
        Returns from 0 to n matching products.
        '''
        try:
            url = 'http://api.zbozi.cz/1/products.json'
            r = requests.get(url, params={'productName': product_name}, auth=(
                settings.STOCK_USERNAME, settings.STOCK_PASSWORD))
        except requests.exceptions.RequestException:
            return None
        else:
            return r.json()

    def _ipv6_score(self, prop_name_to_value_dict):
        points_for_property = {5: ['ipv6_wan_autoconf_dhcp', 'ipv6_wan_autoconf_rdnss',
                                   'ipv6_lan_autoconf_rdnss', 'ipv6_6in4', 'ipv6_6to4',
                                   'ipv6_passthrough', 'ipv6_only_function'],
                               10: ['ipv6_pd', 'ipv6_lan_dhcp', 'ipv6_lan_autoconf_dhcp',
                                    'ipv6_firewall', 'ipv6_6rd'],
                               15: ['ipv6_wan_dhcp']
                               }
        score = 0
        score += self._points_for_properties_score(
            points_for_property, prop_name_to_value_dict)
        return score

    def _functions_score(self, prop_name_to_value_dict):
        points_for_property = {10: ['multimedia_dlna', 'guest_network', 'wlan_a',
                                    'wlan_b', 'wlan_g', 'wlan_n150', 'wlan_n300',
                                    'wlan_ac', 'usb_printer_support', 'usb_disk_support']}
        points_for_usb_ports = (0, 10, 15, 20)
        score = 0
        score += self._points_for_properties_score(
            points_for_property, prop_name_to_value_dict)
        score += points_for_usb_ports[
            max(prop_name_to_value_dict.get('usb_port_number', 0), 3)]
        if prop_name_to_value_dict.get('wan_speed', 0) == 1000:
            score += 10
        if prop_name_to_value_dict.get('lan_speed', 0) == 1000:
            score += 20
        return score

    def _speed_score(self, prop_name_to_value_dict):
        score = 0
        wan_speed = prop_name_to_value_dict.get('wan_speed', 0)
        if wan_speed > 0:
            score += 100 * \
                prop_name_to_value_dict.get('wan_lan_v4_iperf', 0) / wan_speed
            score += 100 * \
                prop_name_to_value_dict.get('wan_lan_v6_iperf', 0) / wan_speed
            return score
        else:
            return 0

    def _points_for_properties_score(self, points_for_property_dict, prop_name_to_value_dict):
        score = 0
        for points, properties in points_for_property_dict.iteritems():
            for property in properties:
                if prop_name_to_value_dict.get(property, False):
                    score += points
        return score


class Author(models.Model):

    slugger = lambda x: unicode(x)

    first_name = models.CharField(max_length=255)
    family_name = models.CharField(max_length=255)
    slug = AutoSlugField(populate_from=slugger, unique=True)

    def __unicode__(self):
        return u"{0} {1}".format(self.first_name, self.family_name)


class ReviewLog(models.Model):

    review = models.OneToOneField(RouterReview, related_name="log")
    log = models.TextField(blank=True)

    def __unicode__(self):
        return u"Log for {0} ({1} lines)".format(self.review,
                                                 len(self.log.splitlines()))


class ReviewQuestion(models.Model):
    review = models.ForeignKey(RouterReview)
    timestamp = models.DateTimeField()
    email = models.EmailField()
    message = models.TextField()

    def __unicode__(self):
        return u"Question on {0} from {1}".format(self.timestamp.date(), self.email)
