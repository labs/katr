from django.conf.urls import patterns, url
from django.utils.translation import ugettext as _
from .views import StaticContentView, HomepageView, RoutersView, RoutersTableView, \
    SearchRoutersView, ReviewDetailView, RouterComparisonView, RouterComparisonModView, \
    ReviewQuestionView
from .backstage_views import UserPageView, ReviewDebugView, ReviewUploadFormView
from .auth_views import LoginView, LogoutView

urlpatterns = patterns(
    'reviews.views',
    url(r'^$', HomepageView.as_view(), name='homepage'),
    url(r'^routers/(?P<tab>bargain|top|price|all|modem)/$',
        RoutersView.as_view(),
        name='routers'),
    url(r'^routers_table/(?P<tab>bargain|top|price|all|modem)/$',
        RoutersTableView.as_view(),
        name='routers_table'),
    url(r'^search/$',
        SearchRoutersView.as_view(),
        name='search'),
    url(r'^review/(?P<slug>[-\w]+)/$',
        ReviewDetailView.as_view(),
        name='review'),
    url(r'^review/(?P<slug>[-\w]+)/comparison/(?P<action>add|remove)/$',
        RouterComparisonModView.as_view(),
        name='router_comparison_mod'),
    url(r'^review_question/(?P<slug>[-\w]+)/$',
        ReviewQuestionView.as_view(),
        name='review_question'),
    url(r'^router_comparison/$',
        RouterComparisonView.as_view(),
        name='router_comparison'),
    url(r'^methodology/$',
        StaticContentView.as_view(
            header_text=_('Methodology'),
            text_slug='methodology_html',
            active_tab='methodology'),
        name='methodology'),
    url(r'^seal/$',
        StaticContentView.as_view(
            header_text=_('Seal of the CZ.NIC Labs'),
            text_slug='seal_html',
            active_tab='seal'),
        name='seal'),
    url(r'^ipv6/$',
        StaticContentView.as_view(
            header_text=_('Why IPv6 router?'),
            text_slug='ipv6_html',
            active_tab='ipv6'),
        name='ipv6'),
    url(r'^about/$',
        StaticContentView.as_view(
            header_text=_('About'),
            text_slug='aboutpage_abouttext',
            active_tab='about'),
        name='about'),
    (r'^setlang/(?P<lang>[a-z]{2,3})/$', 'set_language'),
    # backstage stuff
    url(r'^backstage/login/$',
        LoginView.as_view(),
        name='login_page'),
    url(r'^backstage/logout/$',
        LogoutView.as_view(),
        name='logout_page'),
    url(r'^backstage/user_page/$',
        UserPageView.as_view(),
        name='user_page'),
    url(r'^backstage/review_upload/$',
        ReviewUploadFormView.as_view(),
        name='review_upload'),
    url(r'^backstage/review_debug/(?P<slug>[-\w]+)/$',
        ReviewDebugView.as_view(),
        name='review_debug'),
)
