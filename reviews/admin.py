import models
from django.contrib import admin

admin.site.register(models.RouterReview)
admin.site.register(models.Author)
admin.site.register(models.ReviewQuestion)
