# Django imports
from django.http import HttpResponseRedirect
from django.core.cache import cache
from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.generic import TemplateView, DetailView, FormView

# Global imports
from cStringIO import StringIO
from logging import StreamHandler, getLogger, Formatter

# Local imports
from .models import RouterReview, ReviewLog
from .mixins import BaseCacheControlMixin, LoginRequiredMixin
from .forms import ReviewUploadForm
from .review_import import import_csv_file


class UserPageView(LoginRequiredMixin, BaseCacheControlMixin, TemplateView):
    template_name = "backstage/user_page.html"


class ReviewDebugView(LoginRequiredMixin, BaseCacheControlMixin, DetailView):
    template_name = 'backstage/review_debug.html'
    model = RouterReview


class ReviewUploadFormView(LoginRequiredMixin, BaseCacheControlMixin, FormView):
    template_name = 'backstage/review_upload.html'
    form_class = ReviewUploadForm

    def form_valid(self, form):
        error = ''
        log = StringIO()
        handler = StreamHandler(log)
        handler.setFormatter(Formatter("%(levelname)s:%(message)s"))
        logger = getLogger("katr.import")
        logger.addHandler(handler)
        try:
            review = import_csv_file(self.request.FILES['review_file'], logger)
        except Exception as exc:
            error = exc
        else:
            if review:
                # make review unpublished by default
                review.published = False
                review.save()
                log_obj, _new = ReviewLog.objects.get_or_create(review=review)
                log_obj.log = log.getvalue()
                log_obj.save()
                # invalidate cache for this review
                table_cache_key = "reviews__flat_prop_list__{0}".format(
                    review.slug)
                cache.delete(table_cache_key)
                for lang in settings.LANGUAGES:
                    cache_key = "reviews__filter_search__{0}".format(lang[0])
                    cache.delete(cache_key)
                cache.delete("reviews__latest_reviews")
                return HttpResponseRedirect(reverse('review_debug', args=(review.slug,)))
            else:
                error = log.getvalue()
        form = ReviewUploadForm()
        return self.render_to_response(dict(form=form, error=error))
