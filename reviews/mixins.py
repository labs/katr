# Django imports
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_control
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.views.generic.base import ContextMixin
from django.conf import settings

# Local imports
from static_content.models import StaticText
from properties.models import PropertyList
from .models import RouterReview


class BaseContextMixin(ContextMixin):
    top_list_rules = settings.TOP_LIST_RULES

    def get_context_data(self, **kwargs):
        context = super(BaseContextMixin, self).get_context_data(**kwargs)
        context.update(self.selected_reviews())
        return context

    def selected_reviews(self):
        selected_reviews = self.request.session.get('comparison_reviews', [])
        selected_review_count = len(selected_reviews)
        return dict(selected_review_count=selected_review_count,
                    selected_reviews=selected_reviews)


class BaseCacheControlMixin(object):

    @method_decorator(cache_control(no_cache=True))
    def dispatch(self, *args, **kwargs):
        return super(BaseCacheControlMixin, self).dispatch(*args, **kwargs)


class LoginRequiredMixin(object):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


class StaticTextMixin(BaseCacheControlMixin):

    def get_static_text(self, key):
        static_text = cache.get(key)
        if static_text is None:
            try:
                static_text_object = StaticText.objects.get(name=key)
            except StaticText.DoesNotExist:
                static_text = "!! MISSING {0} !!".format(key)
            else:
                static_text = static_text_object.text
                cache.set(key, static_text, timeout=600)
        return static_text


class PublicReviewMixin(object):

    def get_public_reviews(self, cache_key='reviews__reviews', select_related=True):
        if self.request.user.is_staff:
            # Don't set cache for staff users
            reviews = RouterReview.objects.all()
        else:
            reviews = cache.get(cache_key)
            if reviews is None:
                reviews = RouterReview.objects.filter(published=True)
                if select_related:
                    reviews = reviews.select_related()
                cache.set(cache_key, reviews, 60)
        return reviews

    def add_prev_header_and_parity(self, table):
        """
        Modify property table to allow decomposition into tbody elements and
        alternating row coloring
        """
        previous_header_level = None
        parity = True
        for row in table:
            row.append(previous_header_level)
            if row[1]:
                previous_header_level = row[2]
                # header row parity is True, which stands for even
                parity = True
            else:
                # alternate parity on property rows
                parity = not parity
            row.append(parity)
        return table

    def get_property_list(self, name='short'):
        if name == 'short':
            return PropertyList.objects.get(name='search_filter_props')
        elif name == 'full':
            return PropertyList.objects.get(name='full_review')
        return None

    def get_review_queryset(self, cache_key, reviews, filter_args={}, order_args=[], cache_timeout=60):
        filtered = cache.get(cache_key)
        if filtered is None:
            filtered = reviews.filter(**filter_args).order_by(*order_args)
            cache.set(cache_key, filtered, cache_timeout)
        return filtered

    def get_top_list_queryset(self, reviews, name=''):
        if name in self.top_list_rules.keys():
            cache_key = 'reviews__preview_%s' % name
            return self.get_review_queryset(cache_key=cache_key,
                                            reviews=reviews,
                                            filter_args=self.top_list_rules[
                                                name]['filter'],
                                            order_args=self.top_list_rules[name]['order'])
        else:
            return reviews
