# built-in modules
from datetime import datetime
import csv

# local imports
from .models import RouterReview, Author
from routers.models import Manufacturer, Router, RouterFirmware
from properties.models import Property, PropertyValue, TypedValue,\
    PropertyValueCollection

# django imports
from django.db import transaction


def convert_value(value, value_type):
    if value == "":
        return None
    if value_type == TypedValue.TYPE_BOOL:
        if value.lower() in ('false', 'no'):
            return False
        elif value.lower() in ('true', 'yes'):
            return True
        else:
            raise ValueError("Wrong value for boolean type '%s'" % value)
    elif value_type == TypedValue.TYPE_DATETIME:
        return datetime.strptime(value, "%Y-%m-%d %H:%M:%S")
    elif value_type == TypedValue.TYPE_FLOAT:
        return float(value)
    elif value_type == TypedValue.TYPE_INT:
        return int(round(float(value)))
    elif value_type in (TypedValue.TYPE_IP_ADDR, TypedValue.TYPE_TEXT,
                        TypedValue.TYPE_LOCAL_TEXT, TypedValue.TYPE_URL):
        return value

REVIEW_ATTRS = ("Manufacturer", "Name", "Firmware", "Date", "Author", "Summary",
                "Review", "Hardware revision", "Heureka ID")


@transaction.atomic
def import_csv_file(csv_file, logger):
    # first create a map of all known properties
    prop_name_to_prop = Property.get_name_to_prop_map()
    attrs = dict.fromkeys(REVIEW_ATTRS)
    properties = []
    error = False  # used to monitor if to continue or not
    logger.info("Reading file content and doing pre-check")
    # read the file
    csv_reader = csv.reader(csv_file)
    for row in csv_reader:
        name, val_en, val_cs = [x.decode('utf-8') for x in row[:3]]
        if name == '':
            continue
        if val_en == '':
            logger.warn("Empty value for '%s'", name)
        # possible notes in other columns
        if len(row) >= 5:
            note_en, note_cs = [x.decode('utf-8') for x in row[3:5]]
        else:
            note_en, note_cs = '', ''
        prop = prop_name_to_prop.get(name)
        if prop:
            # this is a known property, great
            logger.debug("Known property '%s' (%s,%s)", prop, val_en,
                         val_cs)
            properties.append((prop, val_en, val_cs, note_en, note_cs))
        elif name in REVIEW_ATTRS:
            # ok, we know what to do with these
            logger.debug("Known review attr '%s' (%s,%s)", name,
                         val_en, val_cs)
            att_val = None
            if name == "Manufacturer":
                att_val, _x = Manufacturer.objects.get_or_create(
                    name=val_en)
            elif name == "Author":
                frst, fam = val_en, val_cs
                att_val, _x = Author.objects.get_or_create(
                    first_name=frst,
                    family_name=fam)
            elif name in ("Name", "Firmware", "Hardware revision", "Score", "Heureka ID"):
                att_val = val_en
            elif name in ("Summary", "Review"):
                att_val = (val_en, val_cs)
            elif name == "Date":
                att_val = datetime.strptime(val_en, "%Y-%m-%d").date()
            attrs[name] = att_val
        else:
            logger.warn("Unknown value '%s' (%s,%s)", name, val_en,
                        val_cs)
    # do some checks
    for key, val in attrs.iteritems():
        if val is None:
            logger.error("Required variable '%s' is missing!", key)
            error = True
    if error:
        logger.warn("Not doing anything because of errors")
        transaction.rollback()
        return
    # normal course of work - create the review instance
    # check for missing properties
    known_properties = set(prop_name_to_prop.keys())
    seen_properties = set([prop[0].name for prop in properties])
    for missing in known_properties - seen_properties:
        logger.warn("Property '%s' is missing in input", missing)
    # at first populate the properties
    logger.info("Creating property collection")
    prop_col = PropertyValueCollection()
    prop_col.save()
    for prop, val_en, val_cs, note_en, note_cs in properties:
        val_en = convert_value(val_en, prop.quantity.value_type)
        if val_en is not None:
            typed_val = PropertyValue(value_type=prop.quantity.value_type,
                                      property=prop,
                                      collection=prop_col,
                                      note_en=note_en,
                                      note_cs=note_cs)
            if typed_val.is_localized():
                if not val_cs and val_en:
                    # no value for cs - use the en value
                    logger.warn("No cs version for localized value '%s', "
                                "using en value", prop.name)
                    val_cs = val_en
                else:
                    val_cs = convert_value(val_cs, prop.quantity.value_type)
                typed_val.set_value(val_en, lang="en")
                typed_val.set_value(val_cs, lang="cs")
            else:
                typed_val.set_value(val_en)
            typed_val.save()
    logger.info("Loaded %d property values",
                len(prop_col.propertyvalue_set.all()))
    logger.debug(prop_col.create_prop_name_to_value_dict())
    # now create the router and firmware
    logger.info("Creating router instance")
    router, _x = Router.objects.get_or_create(
        manufacturer=attrs['Manufacturer'],
        name=attrs['Name'],
        hardware_revision=attrs.get("Hardware revision"))
    logger.info(router)
    logger.info("Creating firmware instance")
    firmware, _x = RouterFirmware.objects.get_or_create(
        router=router,
        name=attrs['Firmware'])
    logger.info(firmware)
    # finally the review itself
    logger.info("Creating review instance")
    for attr in attrs:
        # null values instead of empty strings for integer attrs
        if attr in ("Heureka ID") and attrs[attr] == '':
            attrs[attr] = None
    reviews = RouterReview.objects.filter(router_firmware=firmware,
                                          date=attrs['Date'])
    if reviews.count() >= 1:
        for i in range(1, reviews.count()):
            reviews[i].deep_delete()
        review = reviews[0]
        review.summary_en = attrs['Summary'][0]
        review.summary_cs = attrs['Summary'][1]
        review.text_en = attrs['Review'][0]
        review.text_cs = attrs['Review'][1]
        review.author = attrs['Author']
        review.heureka_id = attrs['Heureka ID']
        review.stock_link = review.get_stock_link(review.heureka_id)
        if review.properties:
            review.properties.delete()
        review.properties = prop_col
    else:
        review = RouterReview(router_firmware=firmware,
                              author=attrs['Author'],
                              date=attrs['Date'],
                              properties=prop_col,
                              summary_en=attrs['Summary'][0],
                              summary_cs=attrs['Summary'][1],
                              text_en=attrs['Review'][0],
                              text_cs=attrs['Review'][1],
                              heureka_id=attrs['Heureka ID'])
        review.stock_link = review.get_stock_link(review.heureka_id)
    review.save()
    review.update_extra_fields(init=True)
    logger.info(review)
    return review
