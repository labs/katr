# Django imports
from django.core.management.base import BaseCommand

# local imports
from reviews.models import RouterReview


class Command(BaseCommand):
    args = ''
    help = 'Updates price of each RouterReview'

    def handle(self, *args, **options):
        for review in RouterReview.objects.all():
            new_price = review.download_price()
            if new_price != review.min_price:
                review.min_price = new_price
                review.bargain = review.compute_bargain()
                review.save()
        self.stdout.write('Successfully updated prices\n')
