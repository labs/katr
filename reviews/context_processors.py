from django.conf import settings


def piwik(context):
    return {'PIWIK_URL': settings.PIWIK_URL,
            'PIWIK_ID': settings.PIWIK_ID}
