# Django imports
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.cache import cache
from django.core.mail import send_mail
from django.utils.translation import check_for_language
from django.conf import settings
from django.utils.translation import ugettext as _, get_language
from django.core.urlresolvers import reverse
from django.views.decorators.cache import cache_control
from django.views.generic import TemplateView, DetailView, View
from django.views.generic.detail import SingleObjectMixin
from django.template.loader import render_to_string
from django.utils.timezone import now

# Local imports
from .models import RouterReview, ReviewQuestion
from .search import get_query
from .mixins import BaseCacheControlMixin, StaticTextMixin, PublicReviewMixin, BaseContextMixin
from .forms import QuestionForm
from properties import models as prop_models

# built in modules
import json
import sys
import traceback


class DefaultTemplateView(BaseContextMixin, TemplateView):
    pass


class DefaultDetailView(BaseContextMixin, DetailView):
    pass


class StaticContentView(StaticTextMixin, DefaultTemplateView):
    template_name = 'static_text.html'
    header_text = ''
    text_slug = ''
    active_tab = ''

    def get_context_data(self, **kwargs):
        context = super(StaticContentView, self).get_context_data(**kwargs)
        context.update(
            header_text=self.header_text,
            static_text=self.get_static_text(self.text_slug),
            active_tab=self.active_tab)
        return context


class HomepageView(StaticTextMixin, PublicReviewMixin, DefaultTemplateView):
    template_name = 'homepage.html'

    def get_context_data(self, **kwargs):
        context = super(HomepageView, self).get_context_data(**kwargs)
        intro_text = self.get_static_text("homepage_introtext")
        ipv6_text = self.get_static_text("homepage_ipv6_text")
        dnssec_text = self.get_static_text("homepage_dnssec_text")
        reviews = self.get_public_reviews()
        reviews_top = self.get_top_list_queryset(reviews, 'top')
        reviews_min_price = self.get_top_list_queryset(reviews, 'min_price')
        reviews_bargain = self.get_top_list_queryset(reviews, 'bargain')
        context.update(
            reviews_top=reviews_top,
            reviews_min_price=reviews_min_price,
            reviews_bargain=reviews_bargain,
            intro_text=intro_text,
            ipv6_text=ipv6_text,
            dnssec_text=dnssec_text)
        return context


class BaseRoutersView(PublicReviewMixin, BaseCacheControlMixin, DefaultTemplateView):

    def get_reviews_with_properties(self, reviews):
        # properties
        prop_list_short = self.get_property_list('short')
        table = prop_list_short.get_flat_child_structure()[1:]
        prop_list_full = self.get_property_list('full')
        table_full = prop_list_full.get_flat_child_structure()[1:]
        properties_json = {}
        for hprop, parent, level, vprop in table_full:
            if vprop:
                properties_json[vprop.name] = dict(name=vprop.name,
                                                   local_name=vprop.local_name,
                                                   type=vprop.quantity.get_type_name(),
                                                   quantity=vprop.quantity.name)
        # reviews
        review_json = {}
        for rev in reviews:
            rev_rec = dict(slug=rev.slug, pk=rev.pk,
                           url=reverse('review', args=(rev.slug,)),
                           name=rev.short_name())
            rev_rec['sort_cols'] = dict(
                score=rev.score, min_price=rev.min_price, has_usb=rev.has_usb,
                lan_ports=rev.lan_ports, ipv6_support=rev.ipv6_support)
            name_to_value = rev.properties.create_prop_name_to_propval_dict()
            props = {}
            for name, value in name_to_value.iteritems():
                if name in properties_json:
                    val = value.get_value()
                    if value.value_type == value.TYPE_BOOL:
                        val = _("Yes") if val else _("No")
                    props[name] = val
            rev_rec['properties'] = props
            # add extra manufacturer property
            rev_rec['properties'][
                'manufacturer'] = rev.router_firmware.router.manufacturer.name
            review_json[rev.slug] = rev_rec
        # add extra property for manufacturer
        properties_json['manufacturer'] = dict(name='manufacturer',
                                               local_name='',
                                               type='bool',
                                               quantity='name')
        table_full = self.add_prev_header_and_parity(table_full)
        return table, table_full, reviews, review_json, properties_json


class RoutersView(BaseRoutersView):
    template_name = 'routers.html'

    def get_context_data(self, tab='all', **kwargs):
        context = super(RoutersView, self).get_context_data(**kwargs)
        tab_names = ['all', 'bargain', 'top', 'price', 'modem']
        if tab in tab_names:
            tab_index = tab_names.index(tab)
        else:
            tab_index = 0
        table = cache.get('property_table')
        if table is None:
            prop_list = self.get_property_list('short')
            table = prop_list.get_flat_child_structure()[1:]
        prop_list_full = self.get_property_list('full')
        table_full = prop_list_full.get_flat_child_structure()[1:]
        table_full = self.add_prev_header_and_parity(table_full)
        context.update(
            tab_index=tab_index,
            routers_base=True,
            table=table,
            table_full=table_full)
        return context


class RoutersTableView(BaseRoutersView):
    template_name = 'routers_table.html'

    def get_context_data(self, tab='all', **kwargs):
        context = super(RoutersTableView, self).get_context_data(**kwargs)
        cache_key = "routers__{0}__{1}".format(tab, get_language())
        cached_value = cache.get(cache_key)
        if cached_value is None:
            reviews = self.get_public_reviews()
            if tab == 'bargain':
                reviews = self.get_top_list_queryset(reviews, 'bargain')
                # slice reviews to greater than or equal to median
                median_limit = (reviews.count() + 1) / 2
                reviews = reviews[:median_limit]
            elif tab == 'top':
                reviews = self.get_top_list_queryset(reviews, 'top')
            elif tab == 'price':
                reviews = self.get_top_list_queryset(reviews, 'min_price')
            elif tab == 'modem':
                # select collections which have property name 'wan_type' and
                # property val_local_text_en is not 'ethernet', but VDSL or
                # ADSL
                collection_ids = prop_models.PropertyValue.objects \
                    .filter(property__name='wan_type') \
                    .exclude(val_local_text_en='ethernet') \
                    .values('collection').distinct()
                reviews = reviews \
                    .filter(properties_id__in=[n['collection'] for n in collection_ids])
            table, table_full, reviews, review_json, properties_json = self.get_reviews_with_properties(
                reviews)
            cache.set(cache_key,
                      (table, table_full, reviews, review_json, properties_json),
                      600)
        else:
            table, table_full, reviews, review_json, properties_json = cached_value
        default_order = [review.slug for review in reviews]
        context.update(
            tab=tab,
            reviews=reviews,
            review_json=json.dumps(review_json),
            properties_json=json.dumps(properties_json),
            default_order=json.dumps(default_order))
        return context


class SearchRoutersView(BaseRoutersView):
    template_name = 'search_results.html'

    def get(self, request, *args, **kwargs):
        """
        Search for routers or manufacturers. Returns router page if one result is matching,
        list of routers otherwise
        """
        print self.request.__dict__.keys()
        context = self.get_context_data(**kwargs)
        query_string = self.request.GET.get('query', None)
        if query_string:
            query = get_query(query_string, ['slug'])
            available_reviews = self.get_public_reviews()
            reviews = available_reviews.filter(query).order_by("-date")
            revcount = reviews.count()
            if revcount == 1:
                return HttpResponseRedirect(reverse('review', args=[reviews[0].slug]))
            elif revcount > 1:
                table, table_full, reviews, review_json, properties_json = self.get_reviews_with_properties(
                    reviews)
                context.update(
                    table=table,
                    reviews=reviews,
                    review_json=json.dumps(review_json),
                    properties_json=json.dumps(properties_json),
                    table_full=table_full,
                    query=query_string,
                    revcount=revcount)
                return self.render_to_response(context)
            else:
                pass
        else:
            query_string = " "
        # return routers() with query attribute
        reviews = self.get_public_reviews()
        table, table_full, reviews, review_json, properties_json = self.get_reviews_with_properties(
            reviews)
        default_order = [rev.slug for rev in reviews]
        context.update(
            table=table,
            reviews=reviews,
            review_json=json.dumps(review_json),
            properties_json=json.dumps(properties_json),
            default_order=json.dumps(default_order),
            table_full=table_full,
            routers_base=True,
            query=query_string)
        self.template_name = 'routers.html'
        return self.render_to_response(context)


class ReviewDetailView(PublicReviewMixin, DefaultDetailView):
    model = RouterReview
    template_name = 'review.html'

    def get_context_data(self, **kwargs):
        context = super(ReviewDetailView, self).get_context_data(**kwargs)
        review = self.get_object()
        table_cache_key = "reviews__flat_prop_list__{0}".format(review.slug)
        has_notes_cache_key = "reviews__prop_hasnotes__{0}".format(review.slug)
        table = cache.get(table_cache_key)
        has_notes = cache.get(has_notes_cache_key)
        if table is None or has_notes is None:
            prop_list = self.get_property_list('full')
            table = review.properties.flatten_property_list(prop_list)
            has_notes = self.postprocess_property_table(table)
            cache.set(has_notes_cache_key, has_notes, 600)
            # table is a list of
            # [property|propertlyList, is_parent, level, propval-instance,
            #  has_notes, prev_header_level, parity]
            table = self.add_prev_header_and_parity(table)
            cache.set(table_cache_key, table, 600)
        form = QuestionForm()
        context.update(
            review=review,
            review_percent_score=100 * review.score / 250,
            table=table[1:],
            has_notes=has_notes,
            form=form)
        return context

    def postprocess_property_table(self, table):
        """
        Modifies the table in place and returns True if there are notes present,
        False otherwise
        """
        has_notes = False
        for row in table:
            if row[3]:
                # check for collapsible property lists
                if type(row[3]) == list:
                    value = ", ".join([propval.property.local_name
                                       for propval in row[3]])
                else:
                    # extract the value
                    value = row[3].get_value()
                    # remap booleans
                    if row[3] and \
                       row[3].value_type == prop_models.TypedModel.TYPE_BOOL:
                        value = _("Yes") if value else _("No")
                    if row[3].note:
                        has_notes = True
            else:
                value = None
            row.append(value)
        return has_notes


class RouterComparisonView(BaseRoutersView):
    template_name = 'router_comparison.html'

    def get_context_data(self, **kwargs):
        context = super(RouterComparisonView, self).get_context_data(**kwargs)
        # full_review property list flat structure - try to get it from cache
        table_cache_key = "reviews__full_review__flat_child_struct"
        table = cache.get(table_cache_key)
        if table is None:
            prop_list = self.get_property_list('full')
            # table is a list of
            # [property|propertyList, is_parent, level, propval-instance,
            #  prev_header_level, parity]
            # ignore first item - "full review" row
            table = prop_list.get_flat_child_structure()[1:]
            table = self.add_prev_header_and_parity(table)
            cache.set(table_cache_key, table, 600)
        # the selected reviews in session
        comp_reviews = self.request.session.get("comparison_reviews", [])
        all_reviews = self.get_public_reviews()
        if comp_reviews:
            reviews = all_reviews.filter(pk__in=comp_reviews)
        else:
            reviews = []
        # a dict for each review
        review_data = []
        for rev in reviews:
            dict_cache_key = "reviews__prop_name_dict__{0}".format(rev.slug)
            name_dict = cache.get(dict_cache_key)
            if name_dict is None:
                name_dict = rev.properties.create_prop_name_to_propval_dict()
                cache.set(dict_cache_key, name_dict, 600)
            review_data.append(name_dict)
        # let's do some processing and convert it into a dict of lists instead
        # the first value is an indicator if there is some difference between
        # the values in the list
        all_keys = set()
        for revd in review_data:
            all_keys |= set(revd.keys())
        review_rows = {}
        for key in all_keys:
            row = []
            for revd in review_data:
                propval = revd.get(key)
                if not propval:
                    val = ""
                else:
                    val = propval.get_value()
                    if propval.value_type == prop_models.TypedModel.TYPE_BOOL:
                        val = _("Yes") if val else _("No")
                row.append(val)
            review_rows[key] = (len(set(row)) > 1,  # there is more than one value
                                tuple(row))
        context.update(
            table=table,
            review_data=review_data,
            reviews=reviews,
            review_rows=review_rows)
        return context


class RouterComparisonModView(BaseCacheControlMixin, SingleObjectMixin, View):
    model = RouterReview

    def get(self, request, slug, action, *args, **kwargs):
        """
        Handles adding and removing comparisons, unobtrusive JS-ready.
        """
        rev = self.get_object()
        session_revs = self.request.session.get('comparison_reviews', [])
        if action == "add" and rev.pk not in session_revs:
            session_revs.append(rev.pk)
        elif action == "remove" and rev.pk in session_revs:
            session_revs.remove(rev.pk)
        self.request.session['comparison_reviews'] = session_revs

        if self.request.is_ajax():
            html = render_to_string(
                "compare_box.html", dict(review=rev, action=action))
            return HttpResponse(json.dumps({'selected_reviews_cnt': len(session_revs),
                                            'html': html,
                                            'slug': rev.slug}),
                                content_type='application/json')
        else:
            return_to = self.request.GET.get("return_to")
            if not return_to:
                return_to = self.request.META.get('HTTP_REFERER', None)
            if not return_to:
                return_to = reverse('review', args=[rev.slug])
            return HttpResponseRedirect(return_to)


class ReviewQuestionView(BaseCacheControlMixin, View):

    def post(self, request, slug, *args, **kwargs):
        form = QuestionForm(request.POST)
        if form.is_valid():
            from_email = form.cleaned_data['email']
            message = form.cleaned_data['message']
            recipient_list = [person[1]
                              for person in settings.QUESTION_RECIPIENTS]
            prefix = '[katr-review-question]'
            subject = prefix + ' ' + slug
            review = RouterReview.objects.get(slug=slug)
            question_model = ReviewQuestion(review=review,
                                            timestamp=now(),
                                            email=from_email,
                                            message=message)
            question_model.save()
            send_mail(subject, message, from_email,
                      recipient_list, fail_silently=False)
        return HttpResponse(json.dumps({"valid": form.is_valid()}),
                            content_type="application/json")


#
# some helper stuff
#
@cache_control(no_cache=True)
def set_language(request, lang):
    go_next = request.REQUEST.get('go_next', None)
    if not go_next:
        go_next = request.META.get('HTTP_REFERER', None)
    if not go_next:
        go_next = '/'
    response = HttpResponseRedirect(go_next)
    if request.method == 'GET':
        lang_code = lang
        if lang_code and check_for_language(lang_code):
            if hasattr(request, 'session'):
                request.session['django_language'] = lang_code
            response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang_code)
    return response


def error500(request):
    data = {}
    if request.user.is_staff:
        # we add some extra into the data
        exc_type, exc_value, tb = sys.exc_info()
        data['traceback'] = "\n".join(traceback.format_tb(tb))
        data['exc_type'] = exc_type.__name__
        data['exc_value'] = unicode(exc_value)
    return render(request, "500.html", data)
