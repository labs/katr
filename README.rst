==============
Router Catalog
==============

Django site presenting reviews of routers supporting IPv6 protocol.
Deployed at `katalogrouteru.cz`_

.. _katalogrouteru.cz: https://www.katalogrouteru.cz/

Deployment
==========

Tested and deployed on Ubuntu 12.04 LTS, PostgreSQL database.

Dependencies
------------

To start working on this project, clone this repository, ``cd`` to project
root directory and install all the dependencies to newly created and
activated virtualenv:

::

    sudo apt-get install libpq-dev libjpeg-dev libjpeg-turbo8-dev libjpeg8-dev \
                         libpng12-dev zlib1g-dev libfreetype6-dev
    pip install -r requirements.txt

Initial setup
-------------

Create database user and database for this project. Update file
``settings_local.py`` according to your system setup. For simplicity, in
``settings.py``, ``INSTALLED_APPS``, comment the line with ``'south'``. You
can always convert apps to South later.

Create database tables:

::

    python manage.py syncdb

And fill initial data:

::

    python fill_in_default_properties.py
    python fill_in_static_content.py

For testing purposes, try to load some sample router reviews:

::

    python import_csv_review.py testdata/*.csv
    python import_csv_review.py data/reviews/*.csv

Router prices
-------------

Czech prices are downloaded from the server `zbozi.cz`_. Set correct
``STOCK_USERNAME`` and ``STOCK_PASSWORD`` in ``settings_local.py``. Get
the actual prices with this command:

::

    python manage.py update_prices


You can install a cron job to update it regularly.

.. _zbozi.cz: http://www.zbozi.cz/

Router pictures
---------------

Pictures of routers may be inserted or updated through django admin
interface. Navigate to some Router instance and add image files in the
form below.

Notes
=====

Translation
-----------

Project is using l10n and i18n, but only cs localization is used for now.
Code to enable English (and possibly other) translations is available in
branch i18n.
